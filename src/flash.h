#pragma once

#include <array>
#include <cstdint>

class Memory;

class Flash {
public:
    enum Flash_mode {
        READY,
        ERASE,
        WRITE,
        BANK
    };

    Flash(Memory* mem) {
        memory_ptr = mem;
        //Start with clean mem
        erase();
    }

    void write(uint32_t addr, uint8_t value);
    uint8_t read(uint32_t addr);

private:
    int flash_bank = 0;
    bool id_mode = false;
    Flash_mode flash_mode;
    int command_id = 0;
    Memory* memory_ptr;

    void erase();
    void erase_sector(uint32_t addr);
};