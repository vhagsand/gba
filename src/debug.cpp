#include <iostream>
#include <sstream>
#include <cstdint>
#include <iomanip>

#include "debug.h"
#include "utils.h"
#include "debug_arm.h"
#include "debug_thumb.h"

void Debug::init()
{
    font = TTF_OpenFont("SpaceMono-Regular.ttf", 18);
}

void Debug::draw()
{
    draw_reg("PC  ", cpu.reg_raw(PC, M_USER), 10, 10);
    draw_reg("R0  ", cpu.reg_raw(R0, M_USER), 160, 10);
    draw_reg("R1  ", cpu.reg_raw(R1, M_USER), 10, 35);
    draw_reg("R2  ", cpu.reg_raw(R2, M_USER), 160, 35);
    draw_reg("R3  ", cpu.reg_raw(R3, M_USER), 10, 60);
    draw_reg("R4  ", cpu.reg_raw(R4, M_USER), 160, 60);
    draw_reg("R5  ", cpu.reg_raw(R5, M_USER), 10, 85);
    draw_reg("R6  ", cpu.reg_raw(R6, M_USER), 160, 85);
    draw_reg("R7  ", cpu.reg_raw(R7, M_USER), 10, 110);
    draw_reg("R8  ", cpu.reg_get(R8), 160, 110);
    draw_reg("R9  ", cpu.reg_get(R9), 10, 135);
    draw_reg("R10 ", cpu.reg_get(R10), 160, 135);
    draw_reg("R11 ", cpu.reg_get(R11), 10, 160);
    draw_reg("R12 ", cpu.reg_get(R12), 160, 160);

    draw_reg2("SP(R13)", cpu.reg_raw(SP, M_USER), 10, 210, M_SYSTEM);
    draw_reg2("SP_fiq ", cpu.reg_raw(SP, M_FIQ), 200, 210, M_FIQ);
    draw_reg2("SP_svc ", cpu.reg_raw(SP, M_SUPERVISOR), 10, 235, M_SUPERVISOR);
    draw_reg2("SP_abt ", cpu.reg_raw(SP, M_ABORT), 200, 235, M_ABORT);
    draw_reg2("SP_irq ", cpu.reg_raw(SP, M_IRQ), 10, 260, M_IRQ);
    draw_reg2("SP_und ", cpu.reg_raw(SP, M_UNDEFINED), 200, 260, M_UNDEFINED);

    draw_reg2("LR(R14)", cpu.reg_raw(LR, M_USER), 10, 285, M_SYSTEM);
    draw_reg2("LR_fiq ", cpu.reg_raw(LR, M_FIQ), 200, 285, M_FIQ);
    draw_reg2("LR_svc ", cpu.reg_raw(LR, M_SUPERVISOR), 10, 310, M_SUPERVISOR);
    draw_reg2("LR_abt ", cpu.reg_raw(LR, M_ABORT), 200, 310, M_ABORT);
    draw_reg2("LR_irq ", cpu.reg_raw(LR, M_IRQ), 10, 335, M_IRQ);
    draw_reg2("LR_und ", cpu.reg_raw(LR, M_UNDEFINED), 200, 335, M_UNDEFINED);

    draw_reg("CPSR     ", cpu.reg_raw(CPSR, M_USER), 10, 360);
    draw_reg2("SPSR_fiq ", cpu.reg_raw(SPSR, M_FIQ), 220, 360, M_FIQ);
    draw_reg2("SPSR_svc ", cpu.reg_raw(SPSR, M_SUPERVISOR), 10, 385, M_SUPERVISOR);
    draw_reg2("SPSR_abt ", cpu.reg_raw(SPSR, M_ABORT), 220, 385, M_ABORT);
    draw_reg2("SPSR_irq ", cpu.reg_raw(SPSR, M_IRQ), 10, 410, M_IRQ);
    draw_reg2("SPSR_und ", cpu.reg_raw(SPSR, M_UNDEFINED), 220, 410, M_UNDEFINED);

    draw_flag("N    ", N, 310, 10);
    draw_flag("Z    ", Z, 310, 35);
    draw_flag("C    ", C, 310, 60);
    draw_flag("V    ", V, 310, 85);
    draw_flag("!IRQ ", IRQ, 310, 110);
    draw_flag("!FIQ ", FIQ, 310, 135);

    std::stringstream line0;
    std::string state = cpu.flag_get(STATE)?"THUMB":"ARM";
    line0 << std::hex << "STATE " << state;
    text(line0.str().c_str(), 310, 160);

    std::stringstream line3;
    int mode = cpu.mode_get();
    std::string mode_name = "";
    switch(mode) {
        case M_USER:
            mode_name = "User";
            break;
        case M_FIQ:
            mode_name = "FIQ";
            break;
        case M_IRQ:
            mode_name = "IRQ";
            break;
        case M_SUPERVISOR:
            mode_name = "Supervisor";
            break;
        case M_ABORT:
            mode_name = "Abort";
            break;
        case M_UNDEFINED:
            mode_name = "Undefined";
            break;
        case M_SYSTEM:
            mode_name = "System";
            break;
        default:
            mode_name = "Error!";
    }
    text(("MODE " + mode_name).c_str(), 10, 185);

    uint32_t pc = cpu.reg_raw(PC, M_USER);;
    if(cpu.flag_get(STATE)) { //THUMB
        if(pc != 0) {
            //draw_op_thumb("  ", pc - 2, 10, 485);
        }
        draw_op_thumb("->", pc, 10, 510);
        draw_op_thumb("  ", pc + 2, 10, 535);
    }
    else {
        if(pc != 0) {
            //draw_op_arm("  ", pc - 4, 10, 485);
        }
        draw_op_arm("->", pc, 10, 510);
        draw_op_arm("  ", pc + 4, 10, 535);
    }
}

void Debug::draw_op_arm(std::string opText, uint32_t pc, int posX, int posY)
{
    std::stringstream line;
    uint32_t op = mem.get32(pc);
    line << std::hex << opText << std::setfill('0') << std::setw(8) << op << " " << get_arm_names(op);
    text(line.str().c_str(), posX, posY);
}

void Debug::draw_op_thumb(std::string opText, uint32_t pc, int posX, int posY)
{
    std::stringstream line;
    uint16_t op = mem.get16(pc);
    line << std::hex << opText << std::setfill('0') << std::setw(4) << op << " " << get_thumb_names(op);
    text(line.str().c_str(), posX, posY);
}

void Debug::draw_reg(std::string regText, uint32_t reg, int posX, int posY)
{
    std::stringstream line;
    line << std::setfill('0')
         << std::hex << regText << std::setw(8) << reg;
    text(line.str().c_str(), posX, posY);
}

void Debug::draw_reg2(std::string regText, uint32_t reg, int posX, int posY, int mode)
{
    int current_mode = cpu.mode_get();
    if(current_mode == M_USER) {
        current_mode = M_SYSTEM;
    }
    std::stringstream line;
    line << std::setfill('0')
         << std::hex << regText << std::setw(8) << reg;
    if(current_mode == mode) {
        text(line.str().c_str(), posX, posY);
    }
    else {
        text(line.str().c_str(), posX, posY, {155, 155, 155, 155});
    }
}

void Debug::draw_flag(std::string flagText, Flags flag, int posX, int posY)
{
    std::stringstream line;
    line << flagText << cpu.flag_get(flag);
    text(line.str().c_str(), posX, posY);
}

void Debug::quit()
{
    TTF_CloseFont(font);
}

void Debug::text(const char* text, int posX, int posY, SDL_Color color)
{
    SDL_Surface* surface = TTF_RenderText_Solid(font, text, color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

    int texW = 0;
    int texH = 0;
    SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);

    SDL_Rect text_rect;
    text_rect.x = posX;
    text_rect.y = posY;
    text_rect.w = texW;
    text_rect.h = texH;

    SDL_RenderCopy(renderer, texture, NULL, &text_rect);

    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}

std::string Debug::get_arm_names(uint32_t opcode)
{
    //4 uppermost bits are conditional, if they match, execute, otherwise return
    uint32_t cond = opcode & 0xF0000000;
    std::string suffix = "";
    switch(cond) {
        case 0x00000000: //Z set
            suffix = "EQ";
            break;
        case 0x10000000: //Z clear
            suffix = "NE";
            break;
        case 0x20000000: //C set
            suffix = "CS";
            break;
        case 0x30000000: //C clear
            suffix = "CC";
            break;
        case 0x40000000: //N set
            suffix = "MI";
            break;
        case 0x50000000: //N clear
            suffix = "PL";
            break;
        case 0x60000000: //V set
            suffix = "VS";
            break;
        case 0x70000000: //V clear
            suffix = "VC";
            break;
        case 0x80000000: //C set and Z clear
            suffix = "HI";
            break;
        case 0x90000000: //C clear and Z set
            suffix = "LS";
            break;
        case 0xA0000000: //N == V
            suffix = "GE";
            break;
        case 0xB0000000: //N != V
            suffix = "LT";
            break;
        case 0xC0000000: //Z clear and (N == V)
            suffix = "GT";
            break;
        case 0xD0000000: //Z set or (N != V)
            suffix = "LE";
            break;
    }

    uint32_t id = opcode & 0xE000000;
    std::string op_name = "Undefined";
    switch(id) {
        case 0x0000000:
        {
            if((opcode & 0xFFFFFF0) == 0x12FFF10) {
                    op_name = bx(opcode);
                }
            else if((opcode & 0x10000F0) == 0x0000090) { //MUL, MLA
                if((opcode & 0x00000F0) == 0x0000090) {
                    if(bit_get(opcode, 23)) { //MULL, MLAL
                        op_name = mull_mlal(opcode);
                    }
                    else {
                        op_name = mul_mla(opcode);
                    }
                }
            }
            else if ((opcode & 0x10000F0) == 0x1000090) {
                op_name = swap(opcode);
            }
            else if (((opcode & 0xF0) == 0xB0) || ((opcode & 0xD0) == 0xD0)){
                op_name = hw_transfer(opcode);
            }
            else { //ALU reg
                    op_name = alu(opcode, false);
            }
            break;
        }
        case 0x2000000: //ALU immediate
            op_name = alu(opcode, true);
            break;
        case 0x4000000: //LDR, STR immediate
            op_name = ldr(opcode, false);
            break;
        case 0x6000000: //LDR, STR register
            op_name = ldr(opcode, true);
            break;
        case 0x8000000: //LDM, STM (PUSH, POP)
            op_name = ldm_stm(opcode);
            break;
        case 0xA000000: //B, BL, BLX
            op_name = branch(opcode);
            break;
        case 0xE000000: //SWI
            op_name = "SWI ";
            break;
        default:
            return op_name;
    }
    op_name.insert(op_name.find(" "), suffix);
    return op_name + "\n";
}

std::string Debug::get_thumb_names(uint16_t opcode)
{
    uint16_t id = opcode & 0xF800;
    std::string op_name = "Undefined";
    switch(id) {
        case 0x0000:
        case 0x0800:
        case 0x1000:
            op_name = shift(opcode);
            break;
        case 0x1800:
            op_name = add_sub(opcode); // Add, sub
            break;
        case 0x2000: //Move, compare
        case 0x2800: //add, substract
        case 0x3000: //add, substract
        case 0x3800: //add, substract
            op_name = mcas_imm(opcode);
            break;
        case 0x4000:
            if(bit_get(opcode, 10)) {
                op_name = hi_reg(opcode);
            }
            else {
                op_name = alu(opcode);
            }
            break;
        case 0x4800:
            op_name = ld_pc(opcode);
            break;
        case 0x5000:
        case 0x5800:
            if(bit_get(opcode, 9)) {
                op_name = ls_ext(opcode);
            }
            else {
                op_name = ls_reg(opcode);
            }
            break;
        case 0x6000:
        case 0x6800:
        case 0x7000:
        case 0x7800:
            op_name = ls_imm(opcode);
            break;
        case 0x8000:
        case 0x8800:
            op_name = ls_hw(opcode);
            break;
        case 0x9000:
        case 0x9800:
            op_name = ls_sp(opcode);
            break;
        case 0xA000:
        case 0xA800:
            op_name = ld(opcode);
            break;
        case 0xB000:
        case 0xB800:
            if(bit_get(opcode, 10)) {
                op_name = push_pop(opcode);
            }
            else {
                op_name = sp_ofs(opcode);
            }
            break;
        case 0xC000:
        case 0xC800:
            op_name = ls_mp(opcode);
            break;
        case 0xD000:
        case 0xD800:
            op_name = b_cond(opcode);
            break;
        case 0xE000:
            op_name = b_uncond(opcode);
            break;
        case 0xF000:
        case 0xF800:
            op_name = bl(opcode);
            break;
    }
    return op_name;
}
