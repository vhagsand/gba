#pragma once

#include <array>
#include <string>
#include "gsl/gsl"
#include "timer.h"
#include "dma.h"
#include "mem_defs.h"
#include "apu.h"
#include "flash.h"

class Memory {

    enum Save_type {
        UNDEFINED,
        SRAM,
        FLASH
    };

public:
    Memory() {
        init();
    }

    void set_apu(APU *apu) {
        this->apu = apu;
    }

    bool halt = false;
    bool stop = false;
    std::string ram_save_filename = "";
    Dma *dma[4];

    uint8_t get8(uint32_t addr);
    void set8(uint32_t addr, uint8_t value);
    uint8_t read8(uint32_t addr);
    void write8(uint32_t addr, uint8_t value);
    uint16_t get16(uint32_t addr);
    void set16(uint32_t addr, uint16_t value);
    uint16_t read16(uint32_t addr);
    void write16(uint32_t addr, uint16_t value);
    uint32_t get32(uint32_t addr);
    void set32(uint32_t addr, uint32_t value);
    uint32_t read32(uint32_t addr);
    void write32(uint32_t addr, uint32_t value);
    void load_bios();
    void load_rom(char* path);
    uint16_t* irq_flags();
    uint8_t* timer_reg();
    void irq_set(int bit);
    void timer_set(int index, Timer *timer);
    void dma_set(int index, Dma *dma);
    gsl::span<uint8_t, 168> sound_regs();
    void save_ram();
    void load_ram();

private:
    uint8_t mem[0xE010000];
    uint32_t last_bios_value = 0xE129F000;
    Timer *timer[4];
    APU *apu;
    bool ram_write = false;
    Save_type save_type;
    Flash* flash;

    void init();
    bool handle_io(uint32_t addr, uint8_t value);
};