#pragma once

#include <array>
#include <cstdint>

uint32_t bit_get(uint32_t value, uint8_t bit);
uint64_t bit_get64(uint64_t value, uint8_t bit);
uint32_t bit_set(uint32_t value, uint8_t bit);
uint32_t bit_clear(uint32_t value, uint8_t bit);
uint32_t sign_extend32(uint32_t data, uint32_t bits);