#include <fstream>
#include <vector>
#include <iostream>
//#include <filesystem>
#include "memory.h"
#include "utils.h"

uint8_t Memory::get8(uint32_t addr)
{
    return mem[addr];
}

void Memory::set8(uint32_t addr, uint8_t value)
{
    mem[addr] = value;
}

uint8_t Memory::read8(uint32_t addr)
{
    uint32_t addr_id = addr & 0xF000000;
    switch(addr_id) {
        case 0x0000000: //BIOS
            // BIOS is read protected, when trying to read from BIOS, the last read value will be returned.
            //return last_bios_value;
            if((addr >= 0x00004000 && addr < 0x02000000) || addr >= 0x10000000) {
                //TODO: Return whats on the bus, e.g recently fetched opcode for ARM, more complicated for THUMB
                std::cout << "Fix read of out bounds\n";
                return 0x0;
            }
            break;
        case 0x2000000: //WRAM
            addr &= 0x303FFFF;
            break;
        case 0x3000000: //WRAM
            addr &= 0x3007FFF;
            break;
        //case 0x4000000: //IO
        //    break;
        case 0x5000000: //Palette RAM
            addr &= 0x50003FF;
            break;
        case 0x6000000: //VRAM
            addr &= 0x601FFFF;
            if(addr >= 0x6018000) {
                addr -= 0x8000;
            }
            break;
        case 0x7000000: //OBJ RAM
            addr &= 0x70003FF;
            break;
        case 0xE000000:
            addr &= 0xE00FFFF;
            switch(save_type) {
                case UNDEFINED:
                    save_type = SRAM;
                    break;
                case FLASH:
                    return flash->read(addr);
                case SRAM:
                    break;
            }
            break;
    }
    return mem[addr];
}

void Memory::write8(uint32_t addr, uint8_t value)
{
    uint32_t addr_id = addr & 0xF000000;
    switch(addr_id) {
        case 0x0000000: //BIOS
            return; //Read only
        case 0x2000000: //WRAM
            addr &= 0x303FFFF;
            break;
        case 0x3000000: //WRAM
            addr &= 0x3007FFF;
            break;
        case 0x4000000: //IO
            if(!handle_io(addr, value)) {
                return;
            }
            break;
        case 0x5000000: //Palette RAM
            addr &= 0x50003FF;
            break;
        case 0x6000000: //VRAM
            addr &= 0x601FFFF;
            if(addr >= 0x6018000) {
                addr -= 0x8000;
            }
            break;
        case 0x7000000: //OBJ RAM
            addr &= 0x70003FF;
            break;
        case 0x8000000: //ROM
        case 0x9000000:
        case 0xA000000:
        case 0xB000000:
        case 0xC000000:
        case 0xD000000:
            return; //Read only
        case 0xE000000:
            addr &= 0xE00FFFF;
            switch(save_type) {
                case UNDEFINED:
                    if(addr == 0xE005555 && value == 0xAA) {
                        save_type = FLASH;
                        flash->write(addr, value);
                    }
                    break;
                case FLASH:
                    flash->write(addr, value);
                    return;
                case SRAM:
                    break;
            }
            ram_write = true;
            break;
    }
    mem[addr] = value;
}

uint16_t Memory::get16(uint32_t addr)
{
    return *(uint16_t *)(mem + addr);
}

void Memory::set16(uint32_t addr, uint16_t value)
{
    *(uint16_t *)(mem + addr) = value;
}

uint16_t Memory::read16(uint32_t addr)
{
    uint16_t value = read8(addr);
    value |= (read8(addr + 1)) << 8;
    return value;
}

void Memory::write16(uint32_t addr, uint16_t value)
{
    write8(addr, (value & 0x00FF));
    write8(addr + 1, (value & 0xFF00) >> 8);
}

uint32_t Memory::get32(uint32_t addr)
{
    return *(uint32_t *)(mem + addr);
}

void Memory::set32(uint32_t addr, uint32_t value)
{
    *(uint32_t *)(mem + addr) = value;
}

uint32_t Memory::read32(uint32_t addr)
{
    uint32_t value = read8(addr);
    value |= ((read8(addr + 1)) << 8);
    value |= ((read8(addr + 2)) << 16);
    value |= ((read8(addr + 3)) << 24);
    return value;
}

void Memory::write32(uint32_t addr, uint32_t value)
{
    write8(addr, (value & 0x000000FF));
    write8(addr + 1, (value & 0x0000FF00) >> 8);
    write8(addr + 2, (value & 0x00FF0000) >> 16);
    write8(addr + 3, (value & 0xFF000000) >> 24);
}

void Memory::init()
{
    //mem[POSTFLG] = 1;
    set16(KEYINPUT, 0x03FF);
    flash = new Flash(this);
}

void Memory::load_bios()
{
    size_t i = 0;
    std::ifstream bios_file ("gba_bios.bin", std::ios::in | std::ios::binary);
    if (bios_file.is_open()) {
        while(!bios_file.eof()) {
            bios_file.read((char *)&mem[i], sizeof(uint8_t));
            i++;
        }
    }
}

void Memory::load_rom(char* path)
{
    size_t i = 0x08000000;
    std::ifstream bios_file (path, std::ios::in | std::ios::binary);
    if (bios_file.is_open()) {
        while(!bios_file.eof()) {
            bios_file.read((char *)&mem[i], sizeof(uint8_t));
            i++;
        }
    }
}

uint16_t* Memory::irq_flags()
{
    return reinterpret_cast<uint16_t *>(mem[IF]);
}

uint8_t* Memory::timer_reg()
{
    return &mem[TM0CNT_L];
}

void Memory::irq_set(int bit)
{
    mem[IF] = bit_set(mem[IF], bit);
}

void Memory::timer_set(int index, Timer *timer)
{
    this->timer[index] = timer;
}

void Memory::dma_set(int index, Dma *dma)
{
    this->dma[index] = dma;
}

gsl::span<uint8_t, 168> Memory::sound_regs()
{
    return {&mem[0x04000000], 168};
}

void Memory::save_ram()
{
    if(ram_write) {
        //std::filesystem::create_directory("saves");
        std::ofstream myfile (ram_save_filename, std::ios::binary);
        if (myfile.is_open()) {
            for(int i = 0xE000000; i < 0xE00FFFF; i++) {
                myfile << mem[i];
            }
            myfile.close();
        }
        else {
            std::cout << "Unable to save RAM";
        }
    }
}

void Memory::load_ram()
{
    std::ifstream ram_file (ram_save_filename, std::ios::binary);
    if (ram_file.is_open()) {
        for(int i = 0xE000000; i < 0xE00FFFF; i++) {
            ram_file.read((char *)&mem[i], sizeof(uint8_t));
        }
        ram_file.close();
    }
}

bool Memory::handle_io(uint32_t addr, uint8_t value)
{
    switch(addr) {
        //Interrupts - Writing one resets the flag
        case IF:
        case IF + 1:
            mem[addr] = (~value) & mem[addr];
            return 0;
        //Timers
        case TM0CNT_L:
            timer[0]->set_start_time(value, false);
            return 0;
        case TM0CNT_L + 1:
            timer[0]->set_start_time(value, true);
            return 0;
        case TM0CNT_H:
            timer[0]->set_control(value);
            break;
        case TM1CNT_L:
            timer[1]->set_start_time(value, false);
            return 0;
        case TM1CNT_L + 1:
            timer[1]->set_start_time(value, true);
            return 0;
        case TM1CNT_H:
            timer[1]->set_control(value);
            break;
        case TM2CNT_L:
            timer[2]->set_start_time(value, false);
            return 0;
        case TM2CNT_L + 1:
            timer[2]->set_start_time(value, true);
            return 0;
        case TM2CNT_H:
            timer[2]->set_control(value);
            break;
        case TM3CNT_L:
            timer[3]->set_start_time(value, false);
            return 0;
        case TM3CNT_L + 1:
            timer[3]->set_start_time(value, true);
            return 0;
        case TM3CNT_H:
            timer[3]->set_control(value);
            break;
        case DMA0CNT_H + 1:
            mem[addr] = value;
            dma[0]->set_data();
            return 0;
        case DMA1CNT_H + 1:
            mem[addr] = value;
            dma[1]->set_data();
            return 0;
        case DMA2CNT_H + 1:
            mem[addr] = value;
            dma[2]->set_data();
            return 0;
        case DMA3CNT_H + 1:
            mem[addr] = value;
            dma[3]->set_data();
            return 0;
        case HALTCNT:
            if(bit_get(value, 7)) {
                stop = true;
            }
            else {
                halt = true;
            }
            return 0;
        case SOUND1CNT_H: // -> length
            apu->load_length_counter_square1(
                value & 0x3f
            );
            mem[addr] = value; // TODO: remove?
            break;
        case SOUND1CNT_X + 1: // -> trigger
            if (value & 0x80) {
                apu->trigger_square1();
            }
            mem[addr] = value;
            break;
        case SOUND2CNT_L: // -> length
            apu->load_length_counter_square2(
                value & 0x3f
            );
            // TODO: read ones?
            break;
        case SOUND2CNT_H + 1: // -> trigger
            if (value & 0x80) {
                apu->trigger_square2();
            }
            break;
        case SOUND4CNT_L: // -> length
            apu->load_length_counter_noise(
                value & 0x3f
            );
            // TODO: read ones?
            break;
        case SOUND4CNT_H + 1: // -> trigger
            if (value & 0x80) {
                apu->trigger_noise();
            }
            break;
        case DISPSTAT:
        {
            uint16_t dispstat = mem[DISPSTAT];
            dispstat &= 0x0007;
            value &= 0xFFF8;
            dispstat |= value;
            break;
        }
        case SOUNDCNT_H:
            // FIFO A reset
            if (value & 0x08) {
                apu->reset_fifo_a();
            }
            // FIFO B reset
            if (value & 0x80) {
                apu->reset_fifo_b();
            }
            break;
        // Last byte of FIFO A
        case FIFO_A_H + 1:
            apu->load_fifo_a(mem[FIFO_A_L]);
            apu->load_fifo_a(mem[FIFO_A_L + 1]);
            apu->load_fifo_a(mem[FIFO_A_H]);
            apu->load_fifo_a(mem[FIFO_A_H + 1]);
            break;
        // Last byte of FIFO B
        case FIFO_B_H + 1:
            apu->load_fifo_b(mem[FIFO_B_L]);
            apu->load_fifo_b(mem[FIFO_B_L + 1]);
            apu->load_fifo_b(mem[FIFO_B_H]);
            apu->load_fifo_b(mem[FIFO_B_H + 1]);
            break;
        case KEYINPUT:
        case KEYINPUT + 1:
        case VCOUNT:
            return 0; // Read only
    }
    return 1;
}