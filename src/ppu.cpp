#include "ppu.h"
#include "utils.h"
#include <iostream>

// TODO:
// -Colors/Palettes
// -Priorities
// -Mosaic
// -Rotate/scroll
// -Special effects (alpha blending/brightness)
// -H-Blank Interval Free
// -Forced Blank
// -Sprites
// -Window

bool Ppu::step(int cycles)
{
    bool ready_draw = false;
    cycle_count += cycles;
    dispstat = mem.get16(DISPSTAT);

    if(mem.stop) {
        return false;
    }

    switch(current_state) {
        case DRAW:
            if(cycle_count > 960) { // Go to H-BLANK
                current_state = HBLANK;
                cycle_count -= 960;
                dispstat = bit_set(dispstat, 1);
                mem.dma[0]->transfer_h_blank();
                mem.dma[1]->transfer_h_blank();
                mem.dma[2]->transfer_h_blank();
                mem.dma[3]->transfer_h_blank();
                if(bit_get(dispstat, 4)) {
                    mem.irq_set(1);
                }
                uint16_t dispcnt = mem.get16(DISPCNT);
                int mode = dispcnt & 0x7;
                switch(mode) {
                    case 0:
                        draw_mode_0(dispcnt);
                        break;
                    case 1:
                        draw_mode_1(dispcnt);
                        break;
                    case 2:
                        draw_mode_2(dispcnt);
                        break;
                    case 3:
                        draw_mode_3();
                        break;
                    case 4:
                        draw_mode_4(dispcnt);
                        break;
                    case 5:
                        draw_mode_5(dispcnt);
                        break;
                }
            }
            break;
        case HBLANK:
            if(cycle_count > 272) {
                cycle_count -= 272;
                if(line_count >= 159) { //End of draw, go to VBLANK
                    current_state = VBLANK_DRAW;
                    dispstat = bit_set(dispstat, 0);
                    ready_draw = true; //Signal to draw screen
                    mem.dma[0]->transfer_v_blank();
                    mem.dma[1]->transfer_v_blank();
                    mem.dma[2]->transfer_v_blank();
                    mem.dma[3]->transfer_v_blank();
                    if(bit_get(dispstat, 3)) {
                        mem.irq_set(0);
                    }
                }
                else { //Go and draw next line
                    current_state = DRAW;
                }
                set_line(line_count + 1);
                dispstat = bit_clear(dispstat, 1);
            }
            break;
        case VBLANK_DRAW:
            if(cycle_count > 960) { //End of VBLANK, loop back
                current_state = VBLANK_HBLANK;
                cycle_count -= 960;
                dispstat = bit_set(dispstat, 1);
            }
            break;
        case VBLANK_HBLANK:
            if(line_count == 227) {
                dispstat = bit_clear(dispstat, 0);
            }
            if(cycle_count > 272) {
                cycle_count -= 272;
                if(line_count >= 227) { //End of VBLANK, loop back
                    current_state = DRAW;
                    set_line(0);
                }
                else {
                    current_state = VBLANK_DRAW;
                    set_line(line_count + 1);
                }
                dispstat = bit_clear(dispstat, 1);
            }
            break;
    }
    mem.set16(DISPSTAT, dispstat);
    return ready_draw;
}

void Ppu::set_line(int count)
{
    line_count = count;
    uint16_t vcount = (dispstat >> 8);
    if(vcount == line_count) {
        dispstat = bit_set(dispstat, 2);
        if(bit_get(dispstat, 5)) {
            mem.irq_set(2);
        }
    }
    else {
        dispstat = bit_clear(dispstat, 2);
    }
    mem.set16(VCOUNT, line_count);
}

void Ppu::draw_mode_0(uint16_t dispcnt)
{
    uint64_t sprites[4][128];
    int length[4] = {0};
    bool obj_map_1d = bit_get(dispcnt, 6);
    bool obj_on = bit_get(dispcnt, 12);

    if(obj_on) {
        for(int k = 127; k >= 0; k--) {
            uint64_t attr = mem.get32(OAM + k * 8);
            attr += ((uint64_t)mem.get32(OAM + k * 8 + 4)) << 32;

            if(attr == 0) {
                continue;
            }

            uint16_t priority = ((attr & 0xC0000000000) >> 42);
            sprites[priority][length[priority]] = attr;
            length[priority]++;
        }
    }

    if(bit_get(dispcnt, 11)) {
        draw_tiles(3);
    }
    if(obj_on) {
        draw_sprites(sprites[3], length[3], obj_map_1d);
    }
    if(bit_get(dispcnt, 10)) {
        draw_tiles(2);
    }
    if(obj_on) {
        draw_sprites(sprites[2], length[2], obj_map_1d);
    }
    if(bit_get(dispcnt, 9)) {
        draw_tiles(1);
    }
    if(obj_on) {
        draw_sprites(sprites[1], length[1], obj_map_1d);
    }
    if(bit_get(dispcnt, 8)) {
        draw_tiles(0);
    }
    if(obj_on) {
        draw_sprites(sprites[0], length[0], obj_map_1d);
    }
}

void Ppu::draw_mode_1(uint16_t dispcnt)
{
    if(bit_get(dispcnt, 10)) {
        draw_tiles_aff(2);
    }
    if(bit_get(dispcnt, 9)) {
        draw_tiles(1);
    }
    if(bit_get(dispcnt, 8)) {
        draw_tiles(0);
    }
}

void Ppu::draw_mode_2(uint16_t dispcnt)
{
    if(bit_get(dispcnt, 11)) {
        draw_tiles_aff(3);
    }
    if(bit_get(dispcnt, 10)) {
        draw_tiles_aff(2);
    }
}

void Ppu::draw_mode_3()
{
    for(int i = 0; i < 240; i++) {
        uint16_t pixel = ((line_count * 240) + i);
        uint16_t data = mem.get16(VRAM + (pixel * 2));
        screen_buffer[pixel] = data;
    }
}

void Ppu::draw_mode_4(uint16_t dispcnt)
{
    uint32_t start = VRAM;
    if(bit_get(dispcnt, 4)) {
        start = VRAM;
    }
    for(int i = 0; i < 240; i++) {
        uint16_t pixel = ((line_count * 240) + i);
        uint8_t palette = mem.get8(start + pixel);
        if(palette != 0) {
            uint16_t data = mem.get16(BG_PALETTE + (palette * 2));
            screen_buffer[pixel] = data;
        }
        else { //TODO: Hack, BG0 palette 0 should be used as background
            screen_buffer[pixel] = 0x0;
        }
    }
}

void Ppu::draw_mode_5(uint16_t dispcnt)
{
    //TODO: Implement screen shift and x/y limit properly
    uint32_t start = VRAM;
    if(bit_get(dispcnt, 4)) {
        start = VRAM;
    }
    for(int i = 0; i < 160; i++) {
        uint16_t pixel = ((line_count * 160) + i);
        uint16_t data = mem.get16(start + (pixel * 2));
        screen_buffer[pixel] = data;
    }
}

void Ppu::draw_tiles(int bg_index)
{
    uint16_t bgcnt = 0;
    uint16_t bghofs = 0;
    uint16_t bgvofs = 0;
    switch(bg_index) {
        case 0:
            bgcnt = mem.get16(BG0CNT);
            bghofs = mem.get16(BG0HOFS);
            bgvofs = mem.get16(BG0VOFS);
            break;
        case 1:
            bgcnt = mem.get16(BG1CNT);
            bghofs = mem.get16(BG1HOFS);
            bgvofs = mem.get16(BG1VOFS);
            break;
        case 2:
            bgcnt = mem.get16(BG2CNT);
            bghofs = mem.get16(BG2HOFS);
            bgvofs = mem.get16(BG2VOFS);
            break;
        case 3:
            bgcnt = mem.get16(BG3CNT);
            bghofs = mem.get16(BG3HOFS);
            bgvofs = mem.get16(BG3VOFS);
            break;
    }

    int screen_size = (bgcnt & 0xC000) >> 14;
    bool palette_256 = bit_get(bgcnt, 7);
    uint32_t tile_data = VRAM + (((bgcnt & 0x000C) >> 2) * 0x4000);
    uint32_t map_data = VRAM + (((bgcnt & 0x1F00) >> 8) * 0x800);
    uint16_t hofs_mask;
    uint16_t vofs_mask;

    switch(screen_size) {
        case 0:
            hofs_mask = 0x0FF;
            vofs_mask = 0x0FF;
            break;
        case 1:
            hofs_mask = 0x1FF;
            vofs_mask = 0x0FF;
            break;
        case 2:
            hofs_mask = 0x0FF;
            vofs_mask = 0x1FF;
            break;
        case 3:
            hofs_mask = 0x1FF;
            vofs_mask = 0x1FF;
            break;
    }

    bghofs = bghofs & hofs_mask;
    bgvofs = bgvofs & vofs_mask;
    uint16_t y_coord = (line_count + bgvofs) & vofs_mask;
    uint16_t y_tile = y_coord / 8;

    for(int i = 0; i < 240; i++) {
        uint16_t y_in_tile = y_coord % 8;
        uint16_t x_coord = (i + bghofs) & hofs_mask;
        uint16_t x_tile = x_coord / 8;
        uint32_t x_in_tile = x_coord % 8;
        uint16_t tile;

        if(x_coord >= 256 && y_coord >= 256) {
            tile = mem.get16(map_data + ((x_tile - 32) + ((y_tile - 32) * 32) + 3072) * 2);
        }
        else if(x_coord >= 256) {
            tile = mem.get16(map_data + ((x_tile - 32) + (y_tile * 32) + 1024) * 2);
        }
        else if(y_coord >= 256) {
            tile = mem.get16(map_data + (x_tile + ((y_tile - 32) * 32) + 2048) * 2);
        }
        else {
            tile = mem.get16(map_data + (x_tile + (y_tile * 32)) * 2);
        }

        if(bit_get(tile, 10)) { //Flip X
            x_in_tile = 7 - x_in_tile;
        }
        if(bit_get(tile, 11)) { //Flip Y
            y_in_tile = 7 - y_in_tile;
        }

        uint16_t color;
        if(palette_256) {
            color = draw_256_1(tile, tile_data, y_in_tile, x_in_tile);
        }
        else {
            color = draw_16_16(tile, tile_data, y_in_tile, x_in_tile);
        }
        uint16_t pixel = ((line_count * 240) + i);
        if(color != 0x8000) {
            screen_buffer[pixel] = color;
        }
    }
}

void Ppu::draw_tiles_aff(int bg_index)
{
    (void)bg_index;
}

uint16_t Ppu::draw_256_1(uint16_t tile, uint32_t tile_data, uint16_t y_in_tile, uint32_t x_in_tile)
{
    int tile_num = (tile & 0x03FF) * 64; //64 -> 8-bits per pixel and 8 rows per tile = 8 * 8 = 64
    uint32_t data_addr = tile_data + tile_num + (y_in_tile * 8);
    uint64_t data = mem.get32(data_addr);
    data += ((uint64_t)mem.get32(data_addr + 4) << 32);
    uint64_t palette_mask = (uint64_t)0x00000000000000FF << (x_in_tile * 8);
    uint16_t palette_offset = ((data & palette_mask) >> (x_in_tile * 8)) * 2;
    if(palette_offset != 0) {
        return mem.read16(BG_PALETTE + palette_offset);
    }
    return 0x8000;
}

uint16_t Ppu::draw_16_16(uint16_t tile, uint32_t tile_data, uint16_t y_in_tile, uint32_t x_in_tile)
{
    int tile_num = (tile & 0x03FF) * 32; //32 -> 4-bits per pixel and 8 rows per tile = 4 * 8 = 32
    uint32_t data_addr = tile_data + tile_num + (y_in_tile * 4);
    uint32_t data = mem.get32(data_addr);
    uint32_t palette_mask = 0x0000000F << (x_in_tile * 4);
    uint16_t palette_offset = ((data & palette_mask) >> (x_in_tile * 4)) * 2;
    if(palette_offset != 0) {
        int palette_num = ((tile & 0xF000) >> 12) * 32;
        palette_offset += palette_num;
        return mem.read16(BG_PALETTE + palette_offset);
    }
    return 0x8000;
}

void Ppu::draw_sprites(uint64_t sprites[], int length, bool one_dimensional)
{
    for(int k = 0; k < length; k++) {
        uint64_t sprite = sprites[k];

        int16_t y_coord = sprite & 0xFF;
        if(y_coord > 159) {
            y_coord = sign_extend32(y_coord, 8);
        }
        //bool rot_scale = bit_get(sprite, 8);
        //bool double_size = bit_get(sprite, 9);
        //bool mosaic = bit_get(sprite, 12);
        //bool palette_256 = bit_get(sprite, 13);
        int x_coord = (sprite & 0x1FF0000) >> 16;
        x_coord = sign_extend32(x_coord, 9);
        bool hflip = bit_get(sprite, 28);
        bool vflip = bit_get(sprite, 29);
        uint8_t size = (sprite & 0xC0000000) >> 30;
        size |= (sprite & 0xC000) >> 12;
        uint8_t sizeX = 0;
        uint8_t sizeY = 0;
        switch(size) {
            case 0:
                sizeX = 8;
                sizeY = 8;
                break;
            case 1:
                sizeX = 16;
                sizeY = 16;
                break;
            case 2:
                sizeX = 32;
                sizeY = 32;
                break;
            case 3:
                sizeX = 64;
                sizeY = 64;
                break;
            case 4:
                sizeX = 16;
                sizeY = 8;
                break;
            case 5:
                sizeX = 32;
                sizeY = 8;
                break;
            case 6:
                sizeX = 32;
                sizeY = 16;
                break;
            case 7:
                sizeX = 64;
                sizeY = 32;
                break;
            case 8:
                sizeX = 8;
                sizeY = 16;
                break;
            case 9:
                sizeX = 8;
                sizeY = 32;
                break;
            case 10:
                sizeX = 16;
                sizeY = 32;
                break;
            case 11:
                sizeX = 32;
                sizeY = 64;
                break;
        }
        uint16_t sprite_index = ((sprite & 0x3FF00000000) >> 32) * 32;
        uint16_t palette_index = ((sprite & 0xF00000000000) >> 44) * 32;

        if((y_coord <= line_count) && (y_coord + sizeY > line_count)) {
            uint16_t y_in_tile = line_count - y_coord;
            uint16_t tile_size_x = sizeX / 8;

            if(vflip) { //Flip Y
                y_in_tile = (sizeY - 1) - y_in_tile;
            }

            for(int j = 0; j < tile_size_x; j++) {
                uint32_t data;
                uint16_t x_tile = j;

                if(hflip) { //Flip X
                    x_tile = tile_size_x - 1 - x_tile;
                }

                if(one_dimensional) {
                    data = mem.get32(OVRAM + sprite_index + (x_tile * 32) + ((y_in_tile % 8) * 4) + ((y_in_tile / 8) * 32 * tile_size_x));
                }
                else {
                    data = mem.get32(OVRAM + sprite_index + (x_tile * 32) + ((y_in_tile % 8) * 4) + ((y_in_tile / 8) * 1024));
                }

                for(int i = 0; i < 8; i++) {
                    int x_in_tile = i;
                    if(hflip) { //Flip X
                        x_in_tile = 7 - x_in_tile;
                    }

                    uint32_t palette_mask = 0x0000000F << (x_in_tile * 4);
                    uint16_t palette_offset = ((data & palette_mask) >> (x_in_tile * 4)) * 2;
                    int x_pixel_offset = x_coord + (j * 8) + i;
                    if(x_pixel_offset < 0) {
                        continue;
                    }
                    if(palette_offset != 0) {
                        palette_offset += palette_index;
                        uint16_t pixel = ((line_count * 240) + x_pixel_offset);
                        screen_buffer[pixel] = mem.read16(OB_PALETTE + palette_offset);
                    }
                }
            }
        }
    }
}

uint16_t* Ppu::getPixels()
{
    return screen_buffer;
}