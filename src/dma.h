#pragma once

#include <array>
#include "mem_defs.h"

class Memory;

class Dma {
public:
    Dma(int index, uint16_t* irq, Memory* mem_ptr) {
        dma_index = index;
        irq_register = irq;
        src_reg = DMA0SAD + index * 12;
        dst_reg = DMA0DAD + index * 12;
        cnt_reg = DMA0CNT_L + index * 12;
        ctrl_reg = DMA0CNT_H + index * 12;
        memory_ptr = mem_ptr;
    }

    void set_data();
    void request_fifo_data();
    void transfer_v_blank();
    void transfer_h_blank();

private:
    int dma_index;
    Memory* memory_ptr;
    uint16_t* irq_register;
    uint32_t src_reg;
    uint32_t dst_reg;
    uint32_t cnt_reg;
    uint32_t ctrl_reg;
    uint32_t int_src_reg;
    uint32_t int_dst_reg;
    int dst_ctrl = 0;
    int src_ctrl = 0;
    bool repeat = false;
    bool dma_32 = false;
    bool drq = false;
    int mode = 0;
    bool irq = false;
    bool enabled = false;
    bool old_enabled = false;

    void stop();
    void single_transfer();
};