#pragma once
#include <array>
#include "gsl/gsl"
#include "apu.h"

class Timer {
public:
    Timer(int index, Timer *timer, uint8_t* mem, uint16_t* irq, APU* apu = nullptr)
            : apu(apu)
    {
        count_up_timer = timer;
        timer_index = index;
        memory = mem + (index * 4);
        irq_register = irq;
    }
    void step(int count);
    void set_start_time(uint16_t value, bool high_byte);
    void set_control(uint16_t value);

private:
    uint16_t start_time = 0;
    uint32_t counter;
    Timer* count_up_timer;
    uint8_t* memory;
    int prescale = 1;
    int prescale_cnt = 0;
    bool count_up = false;
    bool irq = false;
    bool enabled = false;
    bool old_enabled = false;
    int timer_index;
    uint16_t* irq_register;
    APU* apu;

    void step_count_up(int count);
    void increment(int count);
};
