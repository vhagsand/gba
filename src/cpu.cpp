#include <iostream>
#include <iomanip>
#include "utils.h"
#include "cpu.h"

bool Cpu::flag_get(Flags flag)
{
    return bit_get(regs[CPSR][0], flag);
}

void Cpu::flag_set(Flags flag, bool on)
{
    if(on) {
        regs[CPSR][0] = bit_set(regs[CPSR][0], flag);
    }
    else {
        regs[CPSR][0] = bit_clear(regs[CPSR][0], flag);
    }
}

Modes Cpu::mode_get()
{
    return (Modes)(regs[CPSR][0] & 0x1F);
}

void Cpu::mode_set(Modes mode)
{
    regs[CPSR][0] &= 0xFFFFFFE0;
    regs[CPSR][0] |= mode;
}

uint32_t Cpu::reg_get(uint32_t reg)
{
    switch(reg) {
        case SP:
        case LR:
        case SPSR:
        {
            int mode = mode_get();
            if(mode == M_USER || mode == M_SYSTEM) {
                return regs[reg][0];
            }
            else {
                return regs[reg][mode - 16];
            }
        }
        case PC:
            if(flag_get(STATE)) {
                return regs[reg][0] + 2; //Add for THUMB prefetch
            }
            else {
                return regs[reg][0] + 4; //Add for ARM prefetch
            }
        case R8:
        case R9:
        case R10:
        case R11:
        case R12:
        {
            int mode = mode_get();
            if(mode == M_FIQ) {
                return regs[reg][M_FIQ - 16];
            }
            else {
                return regs[reg][0];
            }
            break;
        }
        default:
            return regs[reg][0];
    }
}

uint32_t Cpu::reg_raw(Regs reg, Modes mode)
{
    return regs[reg][mode - 16];
}

void Cpu::reg_set(uint32_t reg, uint32_t value)
{
    switch(reg) {
        case SP:
        case LR:
        case SPSR:
        {
            int mode = mode_get();
            if(mode == M_USER || mode == M_SYSTEM) {
                regs[reg][0] = value;
            }
            else {
                regs[reg][mode - 16] = value;
            }
            break;
        }
        case PC:
            if(flag_get(STATE)) {
                value &=  0xFFFFFFFE;
            }
            else {
                value &=  0xFFFFFFFC;
            }
            regs[reg][0] = value;
            break;
        case R8:
        case R9:
        case R10:
        case R11:
        case R12:
        {
            int mode = mode_get();
            if(mode == M_FIQ) {
                regs[reg][M_FIQ - 16] = value;
            }
            else {
                regs[reg][0] = value;
            }
            break;
        }
        default:
            regs[reg][0] = value;
    }
}

int Cpu::step()
{
    exec_interrupts();

    if(mem.stop || mem.halt) {
        return 1;
    }

    //Execute instruction
    if(flag_get(STATE)) {
        return exec_thumb();
    }
    else {
        return exec_arm();
    }
}

void Cpu::init()
{
    mode_set(M_SUPERVISOR);
}

void Cpu::init_no_bios()
{
    regs[R0][0] = 0x00000CA5;
    regs[CPSR][0] = 0x1F;
    regs[SP][M_SUPERVISOR - 16] = 0x03007FE0;
    regs[SP][M_IRQ - 16] = 0x03007FA0;
    regs[SP][0] = 0x03007F00;
    regs[LR][0] = 0x08000000;
    regs[PC][0] = 0x08000000;
}

void Cpu::exec_interrupts()
{
    //Handle interrupts
    if(bit_get(mem.get16(IME), 0) && !bit_get(reg_get(CPSR), IRQ)) { //IEs enabled
        if(mem.get16(IE) & mem.get16(IF)) { //IE triggered
            uint32_t cpsr = reg_get(CPSR);
            mode_set(M_IRQ);
            if(flag_get(STATE)) {
                reg_set(LR, reg_get(PC) + 2); //Store PC
            }
            else {
                reg_set(LR, reg_get(PC)); //Store PC
            }
            regs[PC][0] = 0x18; //Go to interurupt handler
            reg_set(SPSR, cpsr);
            regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE); //ARM mode
            regs[CPSR][0] = bit_set(regs[CPSR][0], IRQ); //Disable interrupts

            mem.halt = false;
            mem.stop = false;
        }
    }
}

int Cpu::exec_arm()
{
    uint32_t opcode = mem.get32(regs[PC][0]);
    regs[PC][0] += 4;

    //4 uppermost bits are conditional, if they match, execute, otherwise return
    uint32_t cond = opcode & 0xF0000000;
    switch(cond) {
        case 0x00000000: //EQ - Z set
            if(!flag_get(Z))
                return 0;
            break;
        case 0x10000000: //NE - Z clear
            if(flag_get(Z))
                return 0;
            break;
        case 0x20000000: //CS - C set
            if(!flag_get(C))
                return 0;
            break;
        case 0x30000000: //CC - C clear
            if(flag_get(C))
                return 0;
            break;
        case 0x40000000: //MI - N set
            if(!flag_get(N))
                return 0;
            break;
        case 0x50000000: //PL - N clear
            if(flag_get(N))
                return 0;
            break;
        case 0x60000000: //VS - V set
            if(!flag_get(V))
                return 0;
            break;
        case 0x70000000: //VC - V clear
            if(flag_get(V))
                return 0;
            break;
        case 0x80000000: //HI - C set and Z clear
            if(!(flag_get(C) && !flag_get(Z)))
                return 0;
            break;
        case 0x90000000: //LS - C clear OR Z set
            if(!(!flag_get(C) || flag_get(Z)))
                return 0;
            break;
        case 0xA0000000: //GE - N == V
            if(flag_get(N) != flag_get(V))
                return 0;
            break;
        case 0xB0000000: //LT - N != V
            if(flag_get(N) == flag_get(V))
                return 0;
            break;
        case 0xC0000000: //GT - Z clear and (N == V)
            if(!(!flag_get(Z) && (flag_get(N) == flag_get(V))))
                return 0;
            break;
        case 0xD0000000: //LE - Z set or (N != V)
            if(!(flag_get(Z) || (flag_get(N) != flag_get(V))))
                return 0;
            break;
        case 0xE0000000: //AL - Always run
            break;
    }

    uint32_t id = opcode & 0xE000000;
    int retval = 0;
    switch(id) {
        case 0x0000000:
        {
            if((opcode & 0xFFFFFF0) == 0x12FFF10) {
                    retval = bx(opcode);
                }
            else if((opcode & 0x10000F0) == 0x0000090) { //MUL, MLA
                if((opcode & 0x00000F0) == 0x0000090) {
                    if(bit_get(opcode, 23)) { //MULL, MLAL
                        retval = mull_mlal(opcode);
                    }
                    else {
                        retval = mul_mla(opcode);
                    }
                }
            }
            else if ((opcode & 0x10000F0) == 0x1000090) {
                retval = swap(opcode);
            }
            else if (((opcode & 0xF0) == 0xB0) || ((opcode & 0xD0) == 0xD0)){
                retval = hw_transfer(opcode);
            }
            else { //ALU reg
                retval = alu(opcode, false);
            }
            break;
        }
        case 0x2000000: //ALU immediate
            retval = alu(opcode, true);
            break;
        case 0x4000000: //LDR, STR immediate
            retval = ldr(opcode, false);
            break;
        case 0x6000000: //LDR, STR register
            retval = ldr(opcode, true);
            break;
        case 0x8000000: //LDM, STM (PUSH, POP)
            retval = ldm_stm(opcode);
            break;
        case 0xA000000: //B, BL, BLX
            retval = b_bl(opcode);
            break;
        case 0xE000000: //SWI
            retval = swi();
            break;
    }
    return retval;
}

int Cpu::mul_mla(uint32_t opcode)
{
    //TODO: Calculate proper timings
    bool A = bit_get(opcode, 21);
    bool S = bit_get(opcode, 20);
    uint32_t Rd = (opcode & 0xF0000) >> 16;
    uint32_t Rn = (opcode & 0xF000) >> 12;
    uint32_t Rs = (opcode & 0xF00) >> 8;
    uint32_t Rm = (opcode & 0xF);
    uint32_t res;

    if(A) { //MLA
        res = reg_get(Rm) * reg_get(Rs) + reg_get(Rn);
        reg_set(Rd, res);
    }
    else { //MUL
        res = reg_get(Rm) * reg_get(Rs);
        reg_set(Rd, res);
    }
    if(S) {
        flag_set(Z, res == 0);
        flag_set(N, res >> 31);
        //C not affected
        //V not affected
    }
    return 2;
}

int Cpu::mull_mlal(uint32_t opcode)
{
    //TODO: Calculate proper timings
    uint32_t Op = opcode & 0x600000;
    bool S = bit_get(opcode, 20);
    uint32_t RdHi = (opcode & 0xF0000) >> 16;
    uint32_t RdLo = (opcode & 0xF000) >> 12;
    uint32_t Rs = (opcode & 0xF00) >> 8;
    uint32_t Rm = (opcode & 0xF);

    switch(Op) {
        case 0x000000: //UMULL
        {
            uint64_t res = reg_get(Rm) * (uint64_t)reg_get(Rs);
            reg_set(RdLo, (uint32_t)res);
            reg_set(RdHi, (uint32_t)(res >> 32));
            if(S) {
                flag_set(Z, res == 0);
                flag_set(N, bit_get64(res, 63));
                //C not affected
                //V not affected
            }
            break;
        }
        case 0x200000: //UMLAL
        {
            uint64_t hi_reg = reg_get(RdHi);
            uint64_t add = reg_get(RdLo) + (hi_reg << 32);
            uint64_t res = reg_get(Rm) * (uint64_t)reg_get(Rs) + add;
            reg_set(RdLo, (uint32_t)res);
            reg_set(RdHi, (uint32_t)(res >> 32));
            if(S) {
                flag_set(Z, res == 0);
                flag_set(N, bit_get64(res, 63));
                //C not affected
                //V not affected
            }
            break;
        }
        case 0x400000: //SMULL
        {
            int32_t a = (int32_t)reg_get(Rm);
            int32_t b = (int32_t)reg_get(Rs);
            int64_t res2 = a * (int64_t)b;
            reg_set(RdLo, (int32_t)res2);
            reg_set(RdHi, (int32_t)(res2 >> 32));
            if(S) {
                flag_set(Z, res2 == 0);
                flag_set(N, bit_get64(res2, 63));
                //C not affected
                //V not affected
            }
            break;
        }
        case 0x600000: //SMLAL
        {
            int32_t a = (int32_t)reg_get(Rm);
            int32_t b = (int32_t)reg_get(Rs);
            uint64_t hi_reg = reg_get(RdHi);
            uint64_t add = reg_get(RdLo) + (hi_reg << 32);
            int64_t res2 = a * (int64_t)b + add;
            reg_set(RdLo, (int32_t)res2);
            reg_set(RdHi, (int32_t)(res2 >> 32));
            if(S) {
                flag_set(Z, res2 == 0);
                flag_set(N, bit_get64(res2, 63));
                //C not affected
                //V not affected
            }
            break;
        }
    }
    return 3;
}

int Cpu::hw_transfer(uint32_t opcode)
{
    //TODO: add cycles for PC + 2
    bool P = bit_get(opcode, 24);
    bool U = bit_get(opcode, 23);
    bool I = bit_get(opcode, 22);
    bool W = bit_get(opcode, 21);
    bool L = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    uint32_t Oh = (opcode & 0xF00) >> 8;
    int op = opcode & 0x60;
    uint32_t Rm = (opcode & 0xF);
    int64_t offset = reg_get(Rm);
    uint32_t address = reg_get(Rn);
    int cycles;

    if(I) {
        offset = (Oh << 4) + Rm;
    }
    if(!U) {
        offset = -offset;
    }

    if(L) {
        uint32_t data = 0;
        address += P * offset; //Pre increment

        switch(op) {
            case 0x20: //LDRH
            {
                int shift = address & 0x1;
                uint32_t address2 = address & ~shift;
                data = mem.read16(address2);
                if(shift) {
                    data = ror32(data, 8);
                }
                break;
            }
            case 0x40: //LDRSB
                data = mem.read8(address);
                data = sign_extend32(data, 8);
                break;
            case 0x60: //LDRSH
            {
                int shift = address & 0x1;
                if(shift) {
                    data = mem.read8(address);
                    data = sign_extend32(data, 8);
                }
                else {
                    data = mem.read16(address);
                    data = sign_extend32(data, 16);
                }
                break;
            }
        }

        address += (1 - P) * offset; //Post increment
        reg_set(Rd, data);
        cycles = 3;
    }
    else { //STRH
        address += P * offset; //Pre increment
        int shift = address & 0x1;
        uint32_t address2 = address & ~shift;
        uint32_t value = reg_get(Rd);
        if(Rd == PC) {
            value += 4;
        }
        mem.write16(address2, value);
        address += (1 - P) * offset; //Post increment
        cycles = 2;
    }

    if(W || !P){
        if(Rn != Rd || !L) {
            reg_set(Rn, address);
        }
    }
    return cycles;
}

int Cpu::swap(uint32_t opcode)
{
    bool B = bit_get(opcode, 22);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    uint32_t Rm = (opcode & 0xF);
    uint32_t address = reg_get(Rn);

    if(B) {
        uint8_t data = mem.read8(address);
        mem.write8(address, reg_get(Rm));
        reg_set(Rd, data);
    }
    else {
        int shift = address & 0x3;
        uint32_t address2 = address & ~shift;
        uint32_t data = mem.read32(address2);
        data = ror32(data, shift * 8);
        uint32_t value = reg_get(Rm);
        if(Rd == PC) {
            value += 4;
        }
        mem.write32(address2, value);
        reg_set(Rd, data);
    }
    return 3;
}

int Cpu::bx(uint32_t opcode)
{
    int32_t Rn = opcode & 0xF;
    int32_t value = reg_get(Rn);

    thumb = bit_get(value, 0);
    if(thumb) {
        regs[CPSR][0] = bit_set(regs[CPSR][0], STATE);
        value -= 1;
    }
    else {
        regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE);
    }
    reg_set(PC, value);
    return 3;
}

int Cpu::alu(uint32_t opcode, bool I)
{
    //TODO: Add timings for shift and PC
    uint32_t op = opcode & 0x1E00000;
    bool S = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    uint32_t res = 0;
    uint32_t Op2;
    uint32_t Rn_reg = reg_get(Rn);
    bool logic_carry = flag_get(C);

    if(I) {
        Op2 = opcode & 0xFF;
        uint32_t Is = (opcode & 0xF00) >> 8;
        if(Is != 0) {
            logic_carry = bit_get(Op2, (Is * 2) - 1);
            Op2 = ror32(Op2, (Is * 2));
        }
    }
    else {
        Op2 = reg_shift(opcode, &logic_carry);
        if(bit_get(opcode, 4) && Rn == PC) {
            Rn_reg += 4;
        }
    }

    switch(op) {
        case 0x0000000: //AND
            res = Rn_reg & Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0200000: //EOR
            res = Rn_reg ^ Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                 setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0400000: //SUB
            res = Rn_reg - Op2;
            if(S) {
                flag_set(C, Rn_reg >= Op2);
                flag_set(V, ((Rn_reg ^ Op2) & (Rn_reg ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0600000: //RSB
            res = Op2 - Rn_reg;
            if(S) {
                flag_set(C, Op2 >= Rn_reg);
                flag_set(V, ((Rn_reg ^ Op2) & (Rn_reg ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0800000: //ADD
            res = Rn_reg + Op2;
            if(S) {
                flag_set(C, ((uint64_t)Rn_reg + Op2) & 0x100000000);
                flag_set(V, (~(Rn_reg ^ Op2) & (Op2 ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0A00000: //ADC
            res = Rn_reg + Op2 + flag_get(C);
            if(S) {
                flag_set(C, ((uint64_t)Rn_reg + Op2 + flag_get(C)) & 0x100000000);
                flag_set(V, (~(Rn_reg ^ Op2) & (Op2 ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0C00000: //SBC
            res = Rn_reg - Op2 + flag_get(C) - 1;
            if(S) {
                flag_set(C, (int64_t)Rn_reg >= ((int64_t)Op2 - (flag_get(C) - 1)));
                flag_set(V, ((Rn_reg ^ Op2) & (Rn_reg ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x0E00000: //RSC
            res = Op2 - Rn_reg + flag_get(C) - 1;
            if(S) {
                flag_set(C, (int64_t)Op2 >= ((int64_t)Rn_reg - (flag_get(C) - 1)));
                flag_set(V, ((Op2 ^ Rn_reg) & (Op2 ^ res)) >> 31);
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x1000000:
            if(S) { //TST
                res = Rn_reg & Op2;
                if(S) {
                    flag_set(C, logic_carry);
                    //V not affected
                    setZNArmAlu(Rd, res);
                }
            }
            else { //MRS CPSR
                if(I) {
                    //INvalid
                }
                else {
                    reg_set(Rd, reg_get(CPSR));
                }
            }
            break;
        case 0x1200000:
            if(S) { //TEQ
                res = Rn_reg ^ Op2;
                if(S) {
                    flag_set(C, logic_carry);
                    //V not affected
                    setZNArmAlu(Rd, res);
                }
            }
            else { //MSR CPRS
                bool full = bit_get(opcode, 16);
                if(mode_get() == M_USER || !full) {
                    Op2 &= 0xF0000000;
                    uint32_t cpsr = reg_get(CPSR);
                    cpsr &= 0x0FFFFFFF;
                    reg_set(CPSR, cpsr | Op2);
                }
                else {
                    reg_set(CPSR, Op2);
                }
            }
            break;
        case 0x1400000:
        {
            if(S) { //CMP
                res = Rn_reg - Op2;
                if(S) {
                    flag_set(C, Rn_reg >= Op2);
                    flag_set(V, ((Rn_reg ^ Op2) & (Rn_reg ^ res)) >> 31);
                    setZNArmAlu(Rd, res);
                }
            }
            else { //MRS SPSR
                if(I) {
                    //Invalid
                }
                else {
                    reg_set(Rd, reg_get(SPSR));
                }
            }
            break;
        }
        case 0x1600000:
            if(S) { //CMN
                res = Rn_reg + Op2;
                if(S) {
                    flag_set(C, ((uint64_t)Rn_reg + Op2) & 0x100000000);
                    flag_set(V, (~(Rn_reg ^ Op2) & (Op2 ^ res)) >> 31);
                    setZNArmAlu(Rd, res);
                }
            }
            else { //MSR SPSR
                bool full = bit_get(opcode, 16);
                if(mode_get() == M_USER || !full) {
                    Op2 &= 0xF0000000;
                    uint32_t spsr = reg_get(SPSR);
                    spsr &= 0x0FFFFFFF;
                    reg_set(SPSR, spsr | Op2);
                }
                else {
                    reg_set(SPSR, Op2);
                }
            }
            break;
        case 0x1800000: //ORR
            res = Rn_reg | Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x1A00000: //MOV
            res = Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x1C00000: //BIC
            res = Rn_reg & ~Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
        case 0x1E00000: //MVN
            res = ~Op2;
            if(S) {
                flag_set(C, logic_carry);
                //V not affected
                setZNArmAlu(Rd, res);
            }
            reg_set(Rd, res);
            break;
    }
    return 1;
}

int Cpu::ldr(uint32_t opcode, bool I)
{
    //TODO: ldr PC +2 cycles
    bool P = bit_get(opcode, 24);
    bool U = bit_get(opcode, 23);
    bool B = bit_get(opcode, 22);
    bool W = bit_get(opcode, 21);
    bool L = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    int64_t offset;
    uint32_t address = reg_get(Rn);
    bool logic_carry;

    if(I) {
        offset = reg_shift(opcode, &logic_carry); //Carry not used
    }
    else {
        offset = opcode & 0xFFF;
    }

    if(!U) {
        offset = -offset;
    }
    if(L) {
        if(B) { //LDRB
            address += P * offset; //Pre increment
            reg_set(Rd, mem.read8(address));
            address += (1 - P) * offset; //Post increment
        }
        else { //LDR
            address += P * offset; //Pre increment
            int shift = address & 0x3;
            uint32_t address2 = address & ~shift;
            uint32_t data = mem.read32(address2);
            data = ror32(data, shift * 8);
            reg_set(Rd, data);
            address += (1 - P) * offset; //Post increment
        }
    }
    else {
        if(B) { //STRB
            address += P * offset; //Pre increment
            mem.write8(address, reg_get(Rd));
            address += (1 - P) * offset; //Post increment
        }
        else { //STR
            address += P * offset; //Pre increment
            int shift = address & 0x3;
            uint32_t address2 = address & ~shift;
            uint32_t value = reg_get(Rd);
            if(Rd == PC) {
                value += 4;
            }
            mem.write32(address2, value);
            address += (1 - P) * offset; //Post increment
        }
    }
    if(W || !P) {
        if(Rn != Rd || !L) {
            reg_set(Rn, address);
        }
    }
    return 3;
}

int Cpu::ldm_stm(uint32_t opcode)
{
    //TODO: ldm PC +2 cycles
    bool P = bit_get(opcode, 24);
    bool U = bit_get(opcode, 23);
    bool S = bit_get(opcode, 22);
    bool W = bit_get(opcode, 21);
    bool L = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint16_t list = opcode & 0xFFFF;
    uint32_t address = reg_get(Rn);
    int cycles = 2;
    bool first = (__builtin_ctz(list) == (int)Rn);

    if(U) { //Increment
        for(int i = 0; i < 16; i++) {
            if(bit_get(list, i)) {
                address += P * 4; //Pre increment
                if(L) { //Load
                    uint32_t value = mem.read32(address);
                    if(S && i == PC) {
                        reg_set(CPSR, reg_get(SPSR));
                    }
                    else if(S) {
                        regs[i][0] = value;
                    }
                    else {
                        reg_set(i, value);
                    }
                }
                else { //store
                    uint32_t value = reg_get(i);
                    if(i == PC) {
                        value += 4;
                    }
                    if(S) {
                        value = reg_raw((Regs)i, M_USER);
                    }
                    else if(i == (int)Rn && !first) {
                        value += (__builtin_popcount(list) * 4);
                    }
                    mem.write32(address, value);
                }
                address += (1 - P) * 4; //Post increment
                cycles++;
            }
        }
    }
    else { //Decrement
        for(int i = 15; i >= 0; i--) {
            if(bit_get(list, i)) {
                address -= P * 4; //Pre decrement
                if(L) { //Load
                    uint32_t value = mem.read32(address);
                    if(S && i == PC) {
                        reg_set(CPSR, reg_get(SPSR));
                    }
                    else if(S) {
                        regs[i][0] = value;
                    }
                    else {
                        reg_set(i, value);
                    }
                }
                else { //store
                    uint32_t value = reg_get(i);
                    if(i == PC) {
                        value += 4;
                    }
                    if(S) {
                        value = reg_raw((Regs)i, M_USER);
                    }
                    else if(i == (int)Rn && !first) {
                        value -= (__builtin_popcount(list) * 4);
                    }
                    mem.write32(address, value);
                }
                address -= (1 - P) * 4; //Post decrement
                cycles++;
            }
        }
    }
    if(list == 0) {
        if(L) {
            reg_set(PC, mem.get32(address));
        }
        else {
            uint32_t value = reg_get(PC);
            value += 4;
            mem.write32(address, value);
        }
        if(U) {
            address += 0x40;
        }
        else {
            address -= 0x40;
        }
    }
    if(W && (!L || !bit_get(list, Rn))) {
        reg_set(Rn, address); //Write back
    }
    return cycles;
}

int Cpu::b_bl(uint32_t opcode)
{
    int32_t offset = (opcode & 0xFFFFFF) << 2;
    offset = sign_extend32(offset, 26);
    bool L = bit_get(opcode, 24);
    if(L) { //BL
        reg_set(LR, reg_get(PC) - 4);
        reg_set(PC, reg_get(PC) + offset);
    }
    else { //B
        reg_set(PC, reg_get(PC) + offset);
    }
    return 3;
}

int Cpu::swi()
{
    uint16_t cpsr = reg_get(CPSR);
    mode_set(M_SUPERVISOR);
    reg_set(LR, reg_get(PC) - 4);
    reg_set(PC, 0x08);
    reg_set(SPSR, cpsr);
    regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE); //ARM mode
    regs[CPSR][0] = bit_set(regs[CPSR][0], IRQ); //Disable interrupts
    return 3;
}

int Cpu::exec_thumb()
{
    uint16_t opcode = mem.get16(regs[PC][0]);
    regs[PC][0] += 2;
    uint16_t id = opcode & 0xF800;
    int retval = 0;

    switch(id) {
        case 0x0000:
        case 0x0800:
        case 0x1000:
            retval = shift(opcode);
            break;
        case 0x1800:
            retval = add_sub(opcode);
            break;
        case 0x2000: //Move, compare
        case 0x2800: //add, substract
        case 0x3000: //add, substract
        case 0x3800: //add, substract
            retval = mcas_imm(opcode);
            break;
        case 0x4000:
            if(bit_get(opcode, 10)) {
                retval = hi_reg(opcode);
            }
            else {
                retval = alu(opcode);
            }
            break;
        case 0x4800:
            retval = ld_pc(opcode);
            break;
        case 0x5000:
        case 0x5800:
            if(bit_get(opcode, 9)) {
                retval = ls_ext(opcode);
            }
            else {
                retval = ls_reg(opcode);
            }
            break;
        case 0x6000:
        case 0x6800:
        case 0x7000:
        case 0x7800:
            retval = ls_imm(opcode);
            break;
        case 0x8000:
        case 0x8800:
            retval = ls_hw(opcode);
            break;
        case 0x9000:
        case 0x9800:
            retval = ls_sp(opcode);
            break;
        case 0xA000:
        case 0xA800:
            retval = ld(opcode);
            break;
        case 0xB000:
        case 0xB800:
            if(bit_get(opcode, 10)) {
                retval = push_pop(opcode);
            }
            else {
                retval = sp_ofs(opcode);
            }
            break;
        case 0xC000:
        case 0xC800:
            retval = ls_mp(opcode);
            break;
        case 0xD000:
        case 0xD800:
            retval = b_cond(opcode);
            break;
        case 0xE000:
            retval = b_uncond(opcode);
            break;
        case 0xF000:
        case 0xF800:
            retval = bl(opcode);
            break;
        default:
            std::cout << "Unimplemented code: " << opcode << "\n";
            break;
    }
    return retval;
}

int Cpu::shift(uint16_t opcode)
{
    uint16_t op = opcode & 0x1800;
    uint16_t imm = (opcode & 0x07C0) >> 6;
    uint32_t Rs = reg_get((opcode & 0x0038) >> 3);
    uint16_t Rd = opcode & 0x0007;
    uint32_t res = 0;
    bool carry = flag_get(C);

    switch(op) {
        case 0x0000: //LSL
            res = lsl(imm, Rs, &carry);
            reg_set(Rd, res);
            break;
        case 0x0800: //LSR
            res = lsr(imm, Rs, &carry);
            reg_set(Rd, res);
            break;
        case 0x1000: //ASR
            res = asr(imm, Rs, &carry);
            reg_set(Rd, res);
            break;
    }
    flag_set(Z, res == 0);
    flag_set(N, res >> 31);
    flag_set(C, carry);
    //No V
    return 1;
}

int Cpu::add_sub(uint16_t opcode)
{
    uint16_t Op = (opcode & 0x0600) >> 9;
    uint16_t Rn = (opcode & 0x01C0) >> 6;
    uint32_t RnReg = reg_get(Rn);
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint64_t RsReg = reg_get(Rs);
    uint16_t Rd = opcode & 0x0007;
    uint32_t res;

    switch(Op) {
        case 0: //ADD
            res = RsReg + RnReg;
            reg_set(Rd, res);
            flag_set(C, (RsReg + RnReg) & 0x100000000);
            flag_set(V, (~(RsReg ^ RnReg) & (RnReg ^ res)) >> 31);
            break;
        case 1: //SUB
            res = RsReg - RnReg;
            reg_set(Rd, res);
            flag_set(C, RsReg >= RnReg);
            flag_set(V, ((RsReg ^ RnReg) & (RsReg ^ res)) >> 31);
            break;
        case 2: //ADD
            res = RsReg + Rn;
            reg_set(Rd, res);
            flag_set(C, (RsReg + Rn) & 0x100000000);
            flag_set(V, (~(RsReg ^ Rn) & (Rn ^ res)) >> 31);
            break;
        case 3: //SUB
            res = RsReg - Rn;
            reg_set(Rd, res);
            flag_set(C, RsReg >= Rn);
            flag_set(V, ((RsReg ^ Rn) & (RsReg ^ res)) >> 31);
            break;
    }
    flag_set(Z, res == 0);
    flag_set(N, res >> 31);
    return 1;
}

int Cpu::mcas_imm(uint16_t opcode)
{
    uint16_t op = opcode & 0x1800;
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint64_t RdReg = reg_get(Rd);
    uint16_t nn = opcode & 0x00FF;
    uint32_t res = 0;

    switch(op) {
        case 0x0000: //MOV
            res = nn;
            reg_set(Rd, res);
            //C not affected
            //V not affected
            break;
        case 0x0800: //CMP
            res = RdReg - nn;
            flag_set(C, RdReg >= nn);
            flag_set(V, ((RdReg ^ nn) & (RdReg ^ res)) >> 31);
            break;
        case 0x1000: //ADD
            res = RdReg + nn;
            reg_set(Rd, res);
            flag_set(C, (RdReg + nn) & 0x100000000);
            flag_set(V, (~(RdReg ^ nn) & (nn ^ res)) >> 31);
            break;
        case 0x1800: //SUB
            res = RdReg - nn;
            reg_set(Rd, res);
            flag_set(C, RdReg >= nn);
            flag_set(V, ((RdReg ^ nn) & (RdReg ^ res)) >> 31);
            break;
    }
    flag_set(Z, res == 0);
    flag_set(N, res >> 31);
    return 1;
}

int Cpu::hi_reg(uint16_t opcode)
{
    uint16_t Op = (opcode & 0x03C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);
    int res;
    int cycles = 1;

    switch(Op) {
        case 1: //ADD
            reg_set(Rd, reg_get(Rd) + reg_get(Rs + 8));
            break;
        case 2: //ADD
            reg_set(Rd + 8, reg_get(Rd + 8) + reg_get(Rs));
            break;
        case 3: //ADD
            reg_set(Rd + 8, reg_get(Rd + 8) + reg_get(Rs + 8));
            break;
        case 5: //CMP
        {
            uint32_t RsReg = reg_get(Rs + 8);
            uint32_t RdReg = reg_get(Rd);
            res = RdReg - RsReg;
            flag_set(Z, res == 0);
            flag_set(N, res >> 31);
            flag_set(C, RdReg >= RsReg);
            flag_set(V, ((RdReg ^ RsReg) & (RdReg ^ res)) >> 31);
            break;
        }
        case 6: //CMP
        {
            uint32_t RsReg = reg_get(Rs);
            uint32_t RdReg = reg_get(Rd + 8);
            res = RdReg - RsReg;
            flag_set(Z, res == 0);
            flag_set(N, res >> 31);
            flag_set(C, RdReg >= RsReg);
            flag_set(V, ((RdReg ^ RsReg) & (RdReg ^ res)) >> 31);
            break;
        }
        case 7: //CMP
        {
            uint32_t RsReg = reg_get(Rs + 8);
            uint32_t RdReg = reg_get(Rd + 8);
            res = RdReg - RsReg;
            flag_set(Z, res == 0);
            flag_set(N, res >> 31);
            flag_set(C, RdReg >= RsReg);
            flag_set(V, ((RdReg ^ RsReg) & (RdReg ^ res)) >> 31);
            break;
        }
        case 9: //MOV
            reg_set(Rd, reg_get(Rs + 8));
            break;
        case 10: //MOV
            reg_set(Rd + 8, reg_get(Rs));
            break;
        case 11: //MOV
            reg_set(Rd + 8, reg_get(Rs + 8));
            break;
        case 12: //BX
        {
            int32_t value = reg_get(Rs);
            thumb = bit_get(value, 0);
            if(thumb) {
                regs[CPSR][0] = bit_set(regs[CPSR][0], STATE);
                value -= 1;
            }
            else {
                regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE);
            }
            reg_set(PC, value);
            cycles += 2;
            break;
        }
        case 13: //BX
        {
            int32_t value = reg_get(Rs + 8);
            thumb = bit_get(value, 0);
            if(thumb) {
                regs[CPSR][0] = bit_set(regs[CPSR][0], STATE);
                value -= 1;
            }
            else {
                regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE);
            }
            reg_set(PC, value);
            cycles += 2;
            break;
        }
    }
    return cycles;
}

int Cpu::alu(uint16_t opcode)
{
    uint16_t Op = (opcode & 0x03C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint32_t RsReg = reg_get(Rs);
    uint16_t Rd = (opcode & 0x0007);
    uint64_t RdReg = reg_get(Rd);
    uint32_t res;
    bool carry = flag_get(C);

    switch(Op) {
        case 0: //AND
            res = RdReg & RsReg;
            reg_set(Rd, res);
            break;
        case 1: //EOR
            res = RdReg ^ RsReg;
            reg_set(Rd, res);
            break;
        case 2: //LSL
            res = lsl(RsReg, RdReg, &carry);
            reg_set(Rd, res);
            flag_set(C, carry);
            //V not affected
            break;
        case 3: //LSR
            if(RsReg == 0) {
                res = lsl(RsReg, RdReg, &carry);
            }
            else {
                res = lsr(RsReg, RdReg, &carry);
            }
            reg_set(Rd, res);
            flag_set(C, carry);
            //V not affected
            break;
        case 4: //ASR
            if(RsReg == 0) {
                res = lsl(RsReg, RdReg, &carry);
            }
            else {
                res = asr(RsReg, RdReg, &carry);
            }
            reg_set(Rd, res);
            flag_set(C, carry);
            //V not affected
            break;
        case 5: //ADC
            res = RdReg + RsReg + flag_get(C);
            reg_set(Rd, res);
            flag_set(C, (RdReg + RsReg) & 0x100000000);
            flag_set(V, (~(RdReg ^ RsReg) & (RsReg ^ res)) >> 31);
            break;
        case 6: //SBC
            res = RdReg - RsReg - !flag_get(C);
            reg_set(Rd, res);
            flag_set(C, RdReg >= RsReg);
            flag_set(V, ((RdReg ^ RsReg) & (RdReg ^ res)) >> 31);
            break;
        case 7: //ROR
            if(RsReg == 0) {
                res = lsl(RsReg, RdReg, &carry);
            }
            else {
                res = ror(RsReg, RdReg, &carry);
            }
            reg_set(Rd, res);
            flag_set(C, carry);
            //V not affected
            break;
        case 8: //TST
            res = RdReg & RsReg;
            break;
        case 9: //NEG
            res = -RsReg;
            reg_set(Rd, res);
            flag_set(C, 0 >= RsReg);
            flag_set(V, ((0 ^ RsReg) & (0 ^ res)) >> 31);
            break;
        case 10: //CMP
            res = RdReg - RsReg;
            flag_set(C, RdReg >= RsReg);
            flag_set(V, ((RdReg ^ RsReg) & (RdReg ^ res)) >> 31);
            break;
        case 11: //CMN
            res = RdReg + RsReg;
            flag_set(C, (RdReg + RsReg) & 0x100000000);
            flag_set(V, (~(RdReg ^ RsReg) & (RsReg ^ res)) >> 31);
            break;
        case 12: //ORR
            res = RdReg | RsReg;
            reg_set(Rd, res);
            break;
        case 13: //MUL
            res = RdReg * RsReg;
            reg_set(Rd, res);
            break;
        case 14: //BIC
            res = RdReg & ~RsReg;
            reg_set(Rd, res);
            break;
        case 15: //MVN
            res = ~RsReg;
            reg_set(Rd, res);
            break;
    }
    flag_set(Z, res == 0);
    flag_set(N, res >> 31);
    return 1;
}

int Cpu::ld_pc(uint16_t opcode)
{
    uint32_t Rd = (opcode & 0x0700) >> 8;
    uint32_t imm = (opcode & 0x00FF) << 2;
    uint32_t pc = bit_clear(reg_get(PC), 1);
    reg_set(Rd, mem.get32(pc + imm));
    return 3;
}

int Cpu::ls_ext(uint16_t opcode)
{
    uint16_t Op = (opcode & 0x0C00);
    uint16_t Ro = (opcode & 0x01C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = opcode & 0x0007;
    int cycles = 3;

    switch(Op) {
        case 0x000: //STRH
            mem.write16(reg_get(Rb) + reg_get(Ro), reg_get(Rd));
            cycles = 2;
            break;
        case 0x400: //LDSB
        {
            int32_t value = mem.read8(reg_get(Rb) + reg_get(Ro));
            value = sign_extend32(value, 8);
            reg_set(Rd, value);
            break;
        }
        case 0x800: //LDRH
        {
            uint32_t address = reg_get(Rb) + reg_get(Ro);
            int shift = address & 0x1;
            uint32_t address2 = address & ~shift;
            uint32_t data = mem.read16(address2);
            if(shift) {
                data = ror32(data, 8);
            }
            reg_set(Rd, data);
            break;
        }
        case 0xC00: //LDRSH
        {
            uint32_t address = reg_get(Rb) + reg_get(Ro);
            int32_t value;
            int shift = address & 0x1;
            if(shift) {
                value = mem.read8(address);
                value = sign_extend32(value, 8);
            }
            else {
                value = mem.read16(address);
                value = sign_extend32(value, 16);
            }
            reg_set(Rd, value);
            break;
        }
    }
    return cycles;
}

int Cpu::ls_reg(uint16_t opcode)
{
    uint16_t Op = opcode & 0x0C00;
    uint16_t Ro = (opcode & 0x01C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);
    int cycles = 3;

    switch(Op) {
        case 0x000: //STR
            mem.write32(reg_get(Rb) + reg_get(Ro), reg_get(Rd));
            cycles = 2;
            break;
        case 0x400: //STRB
            mem.write8(reg_get(Rb) + reg_get(Ro), reg_get(Rd));
            cycles = 2;
            break;
        case 0x800: //LDR
        {
            uint32_t address = reg_get(Rb) + reg_get(Ro);
            int shift = address & 0x3;
            address = address & ~shift;
            uint32_t data = mem.read32(address);
            data = ror32(data, shift * 8);
            reg_set(Rd, data);
            break;
        }
        case 0xC00: //LDRB
            reg_set(Rd, mem.read8(reg_get(Rb) + reg_get(Ro)));
            break;
    }
    return cycles;
}

int Cpu::ls_imm(uint16_t opcode)
{
    uint16_t Op = opcode & 0x1800;
    uint16_t imm = (opcode & 0x07C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);
    int cycles = 3;

    switch(Op) {
        case 0x0000: //STR
            mem.write32(reg_get(Rb) + (imm << 2), reg_get(Rd));
            cycles = 2;
            break;
        case 0x0800: //LDR
        {
            uint32_t address = reg_get(Rb) + (imm << 2);
            int shift = address & 0x3;
            address = address & ~shift;
            uint32_t data = mem.read32(address);
            data = ror32(data, shift * 8);
            reg_set(Rd, data);
            break;
        }
        case 0x1000: //STRB
            mem.write8(reg_get(Rb) + imm, reg_get(Rd));
            cycles = 2;
            break;
        case 0x1800: //LDRB
            reg_set(Rd, mem.read8(reg_get(Rb) + imm));
            break;
    }
    return cycles;
}

int Cpu::ls_hw(uint16_t opcode)
{
    bool L = bit_get(opcode, 11);
    uint16_t imm = ((opcode & 0x07C0) >> 6) << 1;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);
    int cycles = 3;

    if(L) { //LDRH
        uint32_t address = reg_get(Rb) + imm;
        int shift = address & 0x1;
        uint32_t address2 = address & ~shift;
        uint32_t data = mem.read16(address2);
        if(shift) {
            data = ror32(data, 8);
        }
        reg_set(Rd, data);
    }
    else { //STRH
        mem.write16(reg_get(Rb) + imm, reg_get(Rd));
        cycles = 2;
    }
    return cycles;
}

int Cpu::ls_sp(uint16_t opcode)
{
    bool L = bit_get(opcode, 11);
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t imm = (opcode & 0x00FF) << 2;
    int cycles = 3;

    if(L) { //LDR
        uint32_t address = reg_get(SP) + imm;
        int shift = address & 0x3;
        address = address & ~shift;
        uint32_t data = mem.read32(address);
        data = ror32(data, shift * 8);
        reg_set(Rd, data);
    }
    else { //STR
        mem.write32(reg_get(SP) + imm, reg_get(Rd));
        cycles = 2;
    }
    return cycles;
}

int Cpu::ld(uint16_t opcode)
{
    bool sp = bit_get(opcode, 11);
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t imm = (opcode & 0x00FF) << 2;
    uint32_t address;

    if(sp) { //SP
        address = reg_get(SP) + imm;
    }
    else {   //PC
        uint32_t pc = reg_get(PC);
        pc = bit_clear(pc, 1);
        address = pc + imm;
    }
    reg_set(Rd, address);
    return 1;
}

int Cpu::push_pop(uint16_t opcode)
{
    bool L = bit_get(opcode, 11);
    bool R = bit_get(opcode, 8);
    uint16_t imm = opcode & 0x00FF;
    int sp = reg_get(SP);
    int cycles = 2;

    if(L) { //POP - post-increment
        for(int i = 0; i < 8; i++) {
            if(bit_get(imm, i)) {
                reg_set(i, mem.read32(sp));
                sp += 4;
                cycles++;
            }
        }
        if(R) { //POP PC
            uint32_t pc = mem.read32(sp);
            reg_set(PC, bit_clear(pc, 0));
            sp += 4;
            cycles++;
        }
    }
    else { //PUSH - pre-decrement
        if(R) { //PUSH LR
            sp -= 4;
            mem.write32(sp, reg_get(LR));
            cycles++;
        }
        for(int i = 7; i >= 0; i--) {
            if(bit_get(imm, i)) {
                sp -= 4;
                mem.write32(sp, reg_get(i));
                cycles++;
            }
        }
    }
    reg_set(SP, sp);
    return cycles;
}

int Cpu::sp_ofs(uint16_t opcode)
{
    bool S = bit_get(opcode, 7);
    int offset = (opcode & 0x007F) << 2;
    if(S) {
        offset = -offset;
    }
    reg_set(SP, reg_get(SP) + offset);

    return 1;
}

int Cpu::ls_mp(uint16_t opcode)
{
    bool L = bit_get(opcode, 11);
    uint16_t Rb = (opcode & 0x0700) >> 8;
    uint16_t imm = opcode & 0x00FF;
    uint32_t addr = reg_get(Rb);
    int cycles = 2;
    bool first = (__builtin_ctz(imm) == (int)Rb);

    for(int i = 0; i < 8; i++) {
        if(bit_get(imm, i)) {
            if(L) { //LDMIA
                reg_set(i, mem.get32(addr));
            }
            else { //STMIA
                uint32_t value = reg_get(i);
                if(i == PC) {
                    value += 2;
                }
                if(i == (int)Rb && !first) {
                    value += (__builtin_popcount(imm) * 4);
                }
                mem.write32(addr, value);
            }
            addr += 4;
            cycles++;
        }
    }
    if(imm == 0) {
        if(L) {
            reg_set(PC, mem.get32(addr));
        }
        else {
            uint32_t value = reg_get(PC);
            value += 2;
            mem.write32(addr, value);
        }
        addr += 0x40;
    }
    if(!L || !bit_get(imm, Rb)) {
        reg_set(Rb, addr);
    }
    return cycles;
}

int Cpu::b_cond(uint16_t opcode)
{
    uint16_t Op = (opcode & 0x0F00) >> 8;
    int32_t offset = (opcode & 0x00FF) << 1;
    bool do_jump = false;

    switch(Op) {
        case 0: //BEQ
            do_jump = flag_get(Z);
            break;
        case 1: //BNE
            do_jump = !flag_get(Z);
            break;
        case 2: //BCS
            do_jump = flag_get(C);
            break;
        case 3: //BCC
            do_jump = !flag_get(C);
            break;
        case 4: //BMI
            do_jump = flag_get(N);
            break;
        case 5: //BPL
            do_jump = !flag_get(N);
            break;
        case 6: //BVS
            do_jump = flag_get(V);
            break;
        case 7: //BVC
            do_jump = !flag_get(V);
            break;
        case 8: //BHI
            do_jump = flag_get(C) && !flag_get(Z);
            break;
        case 9: //BLS
            do_jump = !flag_get(C) || flag_get(Z);
            break;
        case 10: //BGE
            do_jump = (flag_get(N) && flag_get(V)) || (!flag_get(N) && !flag_get(V));
            break;
        case 11: //BLT
            do_jump = (flag_get(N) && !flag_get(V)) || (!flag_get(N) && flag_get(V));
            break;
        case 12: //BGT
            do_jump = !flag_get(Z) && ((flag_get(N) && flag_get(V)) || (!flag_get(N) && !flag_get(V)));
            break;
        case 13: //BLE
            do_jump = flag_get(Z) || ((flag_get(N) && !flag_get(V)) || (!flag_get(N) && flag_get(V)));
            break;
        case 15: //SWI
        {
            uint16_t cpsr = reg_get(CPSR);
            mode_set(M_SUPERVISOR);
            reg_set(LR, reg_get(PC) - 2);
            reg_set(PC, 0x08);
            reg_set(SPSR, cpsr);
            regs[CPSR][0] = bit_clear(regs[CPSR][0], STATE); //ARM mode
            regs[CPSR][0] = bit_set(regs[CPSR][0], IRQ); //Disable interrupts
            return 3;
        }
    }
    if(do_jump) {
        offset = sign_extend32(offset, 9);
        reg_set(PC, reg_get(PC) + offset);
    }
    return 3;
}

int Cpu::b_uncond(uint16_t opcode)
{
    int16_t offset = (opcode & 0x7FF) << 1;
    offset = sign_extend32(offset, 12);
    reg_set(PC, reg_get(PC) + offset);
    return 3;
}

int Cpu::bl(uint16_t opcode)
{
    uint16_t opcode2 = mem.get16(regs[PC][0]);
    uint32_t imm_h = (opcode & 0x7FF) << 12;
    uint32_t imm_l = (opcode2 & 0x7FF) << 1;
    int32_t offset = imm_h | imm_l;
    offset = sign_extend32(offset, 23);
    uint32_t pc = reg_get(PC);
    reg_set(PC, pc + (offset));
    reg_set(LR, (pc | 1));
    return 4;
}

void Cpu::setZNArmAlu(uint32_t Rd, uint32_t res)
{
    if(Rd == PC) {
        int mode = mode_get();
        if(mode == M_USER || mode == M_SYSTEM) {
            return;
        }
        reg_set(CPSR, reg_get(SPSR));
    }
    else {
        flag_set(Z, res == 0);
        flag_set(N, res >> 31);
    }
}

uint32_t Cpu::reg_shift(uint32_t opcode, bool *logic_carry)
{
    int shift_type = opcode & 0x60;
    bool shift_reg = bit_get(opcode, 4);
    uint32_t Rm = opcode & 0xF;
    uint32_t Rm_reg = reg_get(Rm);
    uint32_t shift;
    uint32_t res = 0;

    if(shift_reg) {
        uint32_t Rs = (opcode & 0xF00) >> 8;
        shift = reg_get(Rs);
        shift &= 0xFF;
        if(shift == 0) {
            shift_type = 0;
        }
        if(Rm == PC) {
            Rm_reg += 4;
        }
    }
    else {
        shift = (opcode & 0xF80) >> 7;
    }

    switch(shift_type) {
        case 0x00: //LSL
            res = lsl(shift, Rm_reg, logic_carry);
            break;
        case 0x20: //LSR
            res = lsr(shift, Rm_reg, logic_carry);
            break;
        case 0x40: //ASR
            res = asr(shift, Rm_reg, logic_carry);
            break;
        case 0x60: //ROR
            res = ror(shift, Rm_reg, logic_carry);
            break;
    }
    return res;
}

uint32_t Cpu::lsl(uint32_t shift, uint32_t value, bool *logic_carry)
{
    uint32_t res;

    if(shift == 0) {
        res = value;
    }
    else if(shift < 32) {
        res = value << shift;
        *logic_carry = bit_get(value, 32 - shift);
    }
    else if(shift == 32) {
        res = 0;
        *logic_carry = (value & 1);
    }
    else {
        res = 0;
        *logic_carry = 0;
    }
    return res;
}

uint32_t Cpu::lsr(uint32_t shift, uint32_t value, bool *logic_carry)
{
    uint32_t res;

    if (shift == 0) {
        res = 0;
        *logic_carry = (value & 0x80000000);
    }
    else if(shift < 32) {
        res = value >> shift;
        *logic_carry = bit_get(value, shift - 1);
    }
    else if(shift == 32) {
        res = 0;
        *logic_carry = (value & 0x80000000);
    }
    else {
        res = 0;
        *logic_carry = 0;
    }
    return res;
}

uint32_t Cpu::asr(uint32_t shift, uint32_t value, bool *logic_carry)
{
    uint32_t res;

    if (shift == 0) {
        *logic_carry = (value & 0x80000000);
        if(*logic_carry) {
            res = 0xFFFFFFFF;
        }
        else {
            res = 0;
        }
    }
    else if(shift < 32) {
        *logic_carry = bit_get(value, shift - 1);
        res = value >> shift;
        res = sign_extend32(res, 32 - shift);
    }
    else {
        *logic_carry = (value & 0x80000000);
        res = logic_carry?1:0;
        if(res) {
            res = sign_extend32(res, 1);
        }
    }
    return res;
}

uint32_t Cpu::ror(uint32_t shift, uint32_t value, bool *logic_carry)
{
    uint32_t res;

    if (shift == 0) {
        bool tmp_carry = bit_get(value, 0);
        res = value >> 1;
        res |= (flag_get(C) << 31);
        *logic_carry = tmp_carry;
    }
    else if(shift < 32) {
        res = ror32(value, shift);
        *logic_carry = bit_get(value, shift - 1);
    }
    else if(shift == 32) {
        res = value;
        *logic_carry = (value & 0x80000000);
    }
    else {
        shift %= 32;
        res = ror32(value, shift);
        *logic_carry = bit_get(value, shift - 1);
    }

    return res;
}

uint32_t Cpu::rol32(uint32_t number, uint32_t count)
{
    const unsigned int mask = (CHAR_BIT * sizeof(number) - 1);
    count &= mask;
    return (number << count) | (number >> ((-count)&mask));
}

uint32_t Cpu::ror32(uint32_t number, uint32_t count)
{
    const unsigned int mask = (CHAR_BIT * sizeof(number) - 1);
    count &= mask;
    return (number >> count) | (number << ((-count)&mask));
}