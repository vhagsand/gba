#pragma once

#include <array>
#include <string>

std::string shift(uint16_t opcode);
std::string add_sub(uint16_t opcode);
std::string mcas_imm(uint16_t opcode);
std::string ld_pc(uint16_t opcode);
std::string hi_reg(uint16_t opcode);
std::string alu(uint16_t opcode);
std::string ls_ext(uint16_t opcode);
std::string ls_reg(uint16_t opcode);
std::string ls_imm(uint16_t opcode);
std::string ls_hw(uint16_t opcode);
std::string ls_sp(uint16_t opcode);
std::string ld(uint16_t opcode);
std::string push_pop(uint16_t opcode);
std::string sp_ofs(uint16_t opcode);
std::string ls_mp(uint16_t opcode);
std::string b_cond(uint16_t opcode);
std::string b_uncond(uint16_t opcode);
std::string bl(uint16_t opcode);