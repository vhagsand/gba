#include <iostream>
#include "dma.h"
#include "utils.h"
#include "memory.h"

void Dma::set_data()
{
    uint16_t ctrl = memory_ptr->get16(ctrl_reg);

    dst_ctrl = (ctrl & 0x60) >> 5;
    src_ctrl = (ctrl & 0x180) >> 7;
    repeat = bit_get(ctrl, 9);
    dma_32 = bit_get(ctrl, 10);
    drq = bit_get(ctrl, 11);
    mode = (ctrl & 0x3000) >> 12;
    irq = bit_get(ctrl, 14);
    enabled = bit_get(ctrl, 15);

    if(enabled && (enabled != old_enabled)) {
        old_enabled = enabled;
        int_src_reg = memory_ptr->get32(src_reg);
        int_dst_reg = memory_ptr->get32(dst_reg);
        if(mode == 0) {
            single_transfer();
        }
    }
    else if(!enabled && (enabled != old_enabled)) {
        stop();
    }
}

void Dma::stop()
{
    uint16_t ctrl = memory_ptr->get16(ctrl_reg);
    enabled = false;
    old_enabled = false;
    ctrl = bit_clear(ctrl, 15);
    memory_ptr->set16(ctrl_reg, ctrl);
}

void Dma::single_transfer()
{
    // src/dst are 16 bit values
    uint32_t dst = int_dst_reg;
    uint32_t src = int_src_reg;
    int dst_mod = 2;
    switch(dst_ctrl) {
        case 1:
            dst_mod = -2;
            break;
        case 2:
            dst_mod = 0;
            break;
        default:
            dst_mod = 2;
    }
    int src_mod = 2;
    switch(src_ctrl) {
        case 1:
            src_mod = -2;
            break;
        case 2:
            src_mod = 0;
            break;
        default:
            src_mod = 2;
    }
    if (dma_32) {
        dst_mod *= 2;
        src_mod *= 2;
        dst &= 0xFFFFFFFC;
        src &= 0xFFFFFFFC;
    }
    else {
        dst &= 0xFFFFFFFE;
        src &= 0xFFFFFFFE;
    }

    int cnt = memory_ptr->get16(cnt_reg);

    for(int i = 0; i < cnt; i++) {
        if (dma_32) {
            memory_ptr->write32(dst, memory_ptr->read32(src));
        } else {
            memory_ptr->write16(dst, memory_ptr->read16(src));
        }

        dst += dst_mod;
        int_dst_reg += dst_mod;

        src += src_mod;
        int_src_reg += src_mod;
    }
    if(repeat) {
        if(dst_ctrl == 3) {
            int_dst_reg = memory_ptr->get16(dst_reg);
        }
    }
    else {
        stop();
    }
}

void Dma::request_fifo_data()
{
    if(enabled && mode == 3) { // Audio FIFO mode
        dst_ctrl = 2;
        single_transfer();
    }
}

void Dma::transfer_v_blank()
{
    if(enabled && mode == 1) {
        single_transfer();
    }
}

void Dma::transfer_h_blank()
{
    if(enabled && mode == 2) {
        single_transfer();
    }
}