#include "utils.h"

uint32_t bit_get(uint32_t value, uint8_t bit)
{
    return (value >> bit) & 1;
}

uint64_t bit_get64(uint64_t value, uint8_t bit)
{
    return (value >> bit) & 1;
}

uint32_t bit_set(uint32_t value, uint8_t bit)
{
    return value | (1 << bit);
}

uint32_t bit_clear(uint32_t value, uint8_t bit)
{
    return value & ~(1 << bit);
}

uint32_t sign_extend32(uint32_t data, uint32_t bits)
{
    uint32_t m = 1u << (bits - 1);
    return (data ^ m) - m;
}