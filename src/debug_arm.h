#pragma once

#include <array>
#include <string>

std::string mul_mla(uint32_t opcode);
std::string mull_mlal(uint32_t opcode);
std::string bx(uint32_t opcode);
std::string hw_transfer(uint32_t opcode);
std::string swap(uint32_t opcode);
std::string alu(uint32_t opcode, bool I);
std::string ldr(uint32_t opcode, bool I);
std::string ldm_stm(uint32_t opcode);
std::string branch(uint32_t opcode);
std::string R2reg(uint32_t R);