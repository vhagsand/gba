#pragma once

#if defined(__APPLE__)
#include <SDL2_ttf/SDL_ttf.h>
#else
#include <SDL_ttf.h>
#include <string>
#endif
#include "cpu.h"

class Debug
{
public:
    Debug(SDL_Renderer* renderer, Memory &mem, Cpu &cpu)
        : mem(mem)
        , cpu(cpu)
        , renderer(renderer) {
        init();
    }

    ~Debug() {
        quit();
    }

    void draw();

private:
    TTF_Font* font;
    Memory &mem;
    Cpu &cpu;
    SDL_Renderer* renderer;

    void text(const char* text, int posX, int posY, SDL_Color color = {255, 255, 255, 255});
    void draw_op_arm(std::string opText, uint32_t pc, int posX, int posY);
    void draw_op_thumb(std::string opText, uint32_t pc, int posX, int posY);
    void draw_reg(std::string regText, uint32_t reg, int posX, int posY);
    void draw_reg2(std::string regText, uint32_t reg, int posX, int posY, int mode);
    void draw_flag(std::string flagText, Flags flag, int posX, int posY);
    void init();
    void quit();
    std::string get_arm_names(uint32_t opcode);
    std::string get_thumb_names(uint16_t opcode);
};