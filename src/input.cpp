#include "input.h"
#include "utils.h"

void Input::set_key(int key)
{
    uint16_t keys = mem.get16(KEYINPUT);
    keys = bit_clear(keys, key);
    mem.set16(KEYINPUT, keys);
    handle_Interrupts(keys);
}

void Input::clear_key(int key)
{
    uint16_t keys = mem.get16(KEYINPUT);
    keys = bit_set(keys, key);
    mem.set16(KEYINPUT, keys);
    handle_Interrupts(keys);
}

void Input::handle_Interrupts(uint16_t keys)
{
    uint16_t key_cnt = mem.get16(KEYCNT);
    if(bit_get(key_cnt, 14)) {
        uint16_t key_int = key_cnt & 0x03FF;
        keys = (~keys) & 0x03FF;
        if(bit_get(key_cnt, 15)) { //AND mode
            if(key_int == keys) {
                mem.irq_set(12);
            }
        }
        else { //OR mode
            if((key_int & keys) > 0) {
                mem.irq_set(12);
            }
        }

    }
}