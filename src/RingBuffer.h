#include <cassert>
#include <vector>

class RingBuffer {
    // Must be power of two
    static constexpr size_t N = 1 << 13; // 8192

public:
    size_t size() {
        if (begin <= end) {
            return end - begin;
        } else {
            return end + N - begin;
        }
    }

    bool is_full() {
        // One slot will go unused because begin == end means empty buffer
        return size() == N - 1;
    }

    void push_back(float x) {
        assert(!is_full());

        buffer[end++] = x;
        end &= N - 1;
    }

    std::vector<float> take_front(size_t n) {
        assert(n <= size());

        std::vector<float> data;
        auto it = back_inserter(data);
        if (begin <= end) {
            std::copy(&buffer[begin], &buffer[end], it);
        } else {
            std::copy(&buffer[begin], std::end(buffer), it);
            std::copy(std::begin(buffer), &buffer[end], it);
        }

        begin += n;
        begin &= N - 1;

        return data;
    }

private:
    float buffer[N];
    size_t begin = 0;
    size_t end = 0;
};
