#pragma once

#define GBA_DEBUG

#include <SDL2/SDL.h>
#include "memory.h"
#include "debug.h"
#include "input.h"
#include "RingBuffer.h"

#define gba_width 240
#define gba_height 160
#define gba_scale 3

void handle_events();
void hande_keys_down(int keycode);
void hande_keys_up(int keycode);
SDL_Renderer* create_window(const char* name, int x, int y, int h, int w);
void draw_debug(Debug& debug);
void pause_toggle();
SDL_Texture* draw_main(uint16_t *screen_buffer, SDL_Texture *texture);
void dump_mem();
SDL_GameController* init_controller(SDL_GameController *controller);
void audio_handler([[maybe_unused]] void*, uint8_t*, int);
void quit_app();

SDL_Window* main_window;
SDL_Window* debug_window;
SDL_Renderer* main_render;
SDL_Renderer* debug_render;
bool quit = false;
bool step = false;
bool pause = true;
bool last_pause = true;
Memory mem;
bool redraw = false;
uint32_t break_address = 0;
Input* input;
RingBuffer sound_buffer;
bool no_sound = false;
std::string title = "";