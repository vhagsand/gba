#include "utils.h"
#include "debug_arm.h"

std::string mul_mla(uint32_t opcode)
{
    std::string op_name = "Undefined";
    bool A = bit_get(opcode, 21);
    uint32_t Rd = (opcode & 0xF0000) >> 16;
    uint32_t Rn = (opcode & 0xF000) >> 12;
    uint32_t Rs = (opcode & 0xF00) >> 8;
    uint32_t Rm = (opcode & 0xF);
    if(A) {
        op_name = "MLA " + R2reg(Rd) + ", " + R2reg(Rm) + ", " +
            R2reg(Rs) + ", " + R2reg(Rn);
    }
    else {
        op_name = "MUL " + R2reg(Rd) + ", " + R2reg(Rm) + ", " + R2reg(Rs);
    }
    return op_name;
}

std::string mull_mlal(uint32_t opcode)
{
    std::string op_name = "Undefined";
    uint32_t Op = opcode & 0x600000;
    uint32_t RdHi = (opcode & 0xF0000) >> 16;
    uint32_t RdLo = (opcode & 0xF000) >> 12;
    uint32_t Rs = (opcode & 0xF00) >> 8;
    uint32_t Rm = (opcode & 0xF);

    switch(Op) {
        case 0x000000:
            op_name = "UMULL " + R2reg(RdLo) + ", " + R2reg(RdHi) + ", " +
            R2reg(Rm) + ", " + R2reg(Rs);
            break;
        case 0x200000:
            op_name = "UMLAL " + R2reg(RdLo) + ", " + R2reg(RdHi) + ", " +
            R2reg(Rm) + ", " + R2reg(Rs);
            break;
        case 0x400000:
            op_name = "SMULL " + R2reg(RdLo) + ", " + R2reg(RdHi) + ", " +
            R2reg(Rm) + ", " + R2reg(Rs);
            break;
        case 0x600000:
            op_name = "SMLAL " + R2reg(RdLo) + ", " + R2reg(RdHi) + ", " +
            R2reg(Rm) + ", " + R2reg(Rs);
            break;
    }
    return op_name;
}

std::string hw_transfer(uint32_t opcode)
{
    std::string op_name = "Undefined";
    bool I = bit_get(opcode, 22);
    bool L = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    uint32_t Oh = (opcode & 0xF00) >> 8;
    uint32_t Rm = (opcode & 0xF);

    if(L) {
        if(I) {
            std::string imm = std::to_string((Oh << 4) + Rm);
            op_name = "LDRH " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + imm + "]";
        }
        else {
            op_name = "LDRH " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + R2reg(Rm) + "]";
        }
    }
    else {
        if(I) {
            std::string imm = std::to_string((Oh << 4) + Rm);
            op_name = "STRH " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + imm + "]";
        }
        else {
            op_name = "STRH " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + R2reg(Rm) + "]";
        }
    }

    return op_name;
}

std::string swap(uint32_t opcode)
{
    std::string op_name = "Undefined";
    bool B = bit_get(opcode, 22);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    uint32_t Rm = opcode & 0xF;

    if(B) {
        op_name = "SWPB " + R2reg(Rd) + ", " + R2reg(Rm) + ", [" + R2reg(Rn) + "]";
    }
    else {
        op_name = "SWP " + R2reg(Rd) + ", " + R2reg(Rm) + ", [" + R2reg(Rn) + "]";
    }

    return op_name;
}

std::string bx(uint32_t opcode)
{
    uint32_t Rn = opcode & 0xF;
    return "BX " + R2reg(Rn);
}

std::string alu(uint32_t opcode, bool I)
{
    std::string op_name = "Undefined";
    uint32_t op = opcode & 0x1E00000;
    bool S = bit_get(opcode, 20);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    std::string Op2;

    if(I) {
        Op2 = std::to_string(opcode & 0xFF);
    }
    else {
        Op2 = R2reg(opcode & 0xF);
    }

    switch(op) {
        case 0x0000000:
            op_name = "AND " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0200000:
            op_name = "EOR " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0400000:
            op_name = "SUB " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0600000:
            op_name = "RSB " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0800000:
            op_name = "ADD " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0A00000:
            op_name = "ADC " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0C00000:
            op_name = "SBC " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x0E00000:
            op_name = "RSC " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x1000000:
            if(S) {
                op_name = "TST " + R2reg(Rn) + ", " + Op2;
            }
            else {
                op_name = "MRS " + R2reg(Rd) + ", CPSR";
            }
            break;
        case 0x1200000:
            if(S) {
                op_name = "TEQ " + R2reg(Rn) + ", " + Op2;
            }
            else {
                uint32_t Rm = opcode & 0xF;
                op_name = "MSR CPSR, " + R2reg(Rm);
            }
            break;
        case 0x1400000:
            if(S) {
                op_name = "CMP " + R2reg(Rn) + ", " + Op2;
            }
            else {
                op_name = "MRS " + R2reg(Rd) + ", SPSR";
            }
            break;
        case 0x1600000:
            if(S) {
                op_name = "CMN " + R2reg(Rn) + ", " + Op2;
            }
            else {
                uint32_t Rm = opcode & 0xF;
                op_name = "MSR SPSR, " + R2reg(Rm);
            }
            break;
        case 0x1800000:
            op_name = "ORR " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x1A00000:
            op_name = "MOV " + R2reg(Rd) + ", " + Op2;
           break;
        case 0x1C00000:
            op_name = "BIC " + R2reg(Rd) + ", " + R2reg(Rn) + ", " + Op2;
            break;
        case 0x1E00000:
            op_name = "MVN " + R2reg(Rn) + ", " + Op2;
            break;
    }
    if(S) {
        op_name.insert(op_name.find(" "), "S");
    }

    if(I) {
        op_name += ", ROR " + std::to_string(opcode & 0xFF);
    }
    else {
        int shift_type = opcode & 0x60;
        bool shift_reg = bit_get(opcode, 4);
        std::string shift;

        if(shift_reg) {
            uint32_t Rs = (opcode & 0xF00) >> 8;
            shift = R2reg(Rs);
        }
        else {
            shift = std::to_string((opcode & 0xF80) >> 7);
        }
        switch(shift_type) {
            case 0x00: //LSL
                op_name += ", LSL " + shift;
                break;
            case 0x20: //LSR
                op_name += ", LSR " + shift;
                break;
            case 0x40: //ASR
                op_name += ", ASR " + shift;
                break;
            case 0x60: //ROR
                op_name += ", ROR " + shift;
                break;
        }
    }
    return op_name;
}

std::string ldr(uint32_t opcode, bool I)
{
    std::string op_name = "Undefined";
    bool load = bit_get(opcode, 20);
    bool byte = bit_get(opcode, 22);
    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t Rd = (opcode & 0xF000) >> 12;
    std::string offset;

    if(I) {
        uint32_t Rm = opcode & 0xF;
        offset = R2reg(Rm);
    }
    else {
        offset = std::to_string(opcode & 0xFFF);
    }

    if(load) {
        if(byte) {
            op_name = "LDRB " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + offset + "]";
        }
        else {
            op_name = "LDR " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + offset + "]";
        }
    }
    else {
        if(byte) {
            op_name = "STRB " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + offset + "]";
        }
        else {
            op_name = "STR " + R2reg(Rd) + ", [" + R2reg(Rn) + ", " + offset + "]";
        }
    }
    return op_name;
}

std::string ldm_stm(uint32_t opcode)
{
    std::string op_name = "Undefined";

    int op = bit_get(opcode, 20) << 2;
    op += bit_get(opcode, 24) << 1;
    op += bit_get(opcode, 23);
    bool S = bit_get(opcode, 22);

    uint32_t Rn = (opcode & 0xF0000) >> 16;
    uint32_t regs = opcode & 0xFFFF;
    if(Rn == 13) {
        switch(op) {
            case 0:
                op_name = "STMED ";
                break;
            case 1:
                op_name = "STMEA ";
                break;
            case 2:
                op_name = "STMFD ";
                break;
            case 3:
                op_name = "STMFA ";
                break;
            case 4:
                op_name = "LDMFA ";
                break;
            case 5:
                op_name = "LDMFD ";
                break;
            case 6:
                op_name = "LDMEA ";
                break;
            case 7:
                op_name = "LDMED ";
                break;
        }
    }
    else {
        switch(op) {
            case 0:
                op_name = "STMDA ";
                break;
            case 1:
                op_name = "STMIA ";
                break;
            case 2:
                op_name = "STMDB ";
                break;
            case 3:
                op_name = "STMIB ";
                break;
            case 4:
                op_name = "LDMDA ";
                break;
            case 5:
                op_name = "LDMIA ";
                break;
            case 6:
                op_name = "LDMDB ";
                break;
            case 7:
                op_name = "LDMIB ";
                break;
        }
    }
    op_name += R2reg(Rn) + ", {" + std::to_string(regs) + "}";
    if(S) {
        op_name += "^";
    }
    return op_name;
}

std::string branch(uint32_t opcode)
{
    std::string op_name = "Undefined";
    int32_t offset = (opcode & 0xFFFFFF) << 2;
    offset = sign_extend32(offset, 26);

    const int negative = (offset & (1 << 23)) != 0;
    if (negative) {
        offset = offset | ~((1 << 18) - 1);
    }
    bool L = bit_get(opcode, 24);
    if(L) {
        op_name = "BL";
    }
    else {
        op_name = "B";
    }
    return op_name + " " + std::to_string(offset);
}

std::string R2reg(uint32_t R)
{
    std::string reg = "";
    if(R <= 12) {
        reg = "R" + std::to_string(R);
    }
    else if(R == 13) {
        reg = "SP";
    }
    else if(R == 14) {
        reg = "LR";
    }
    else if(R == 15) {
        reg = "PC";
    }
    return reg;
}