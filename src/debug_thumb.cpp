#include "utils.h"
#include "debug_thumb.h"
#include "debug_arm.h"
#include "cpu.h"

std::string shift(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t op = opcode & 0x1800;
    uint16_t imm = (opcode & 0x07C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint16_t Rd = opcode & 0x0007;

    switch(op) {
        case 0x0000:
            op_name = "LSL " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + std::to_string(imm);
            break;
        case 0x0800:
            op_name = "LSR " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + std::to_string(imm);
            break;
        case 0x1000:
            op_name = "ASR " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + std::to_string(imm);
            break;
    }
    return op_name;
}

std::string add_sub(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = (opcode & 0x0600) >> 9;
    uint16_t Rn = (opcode & 0x01C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint16_t Rd = opcode & 0x0007;

    switch(Op) {
        case 0:
            op_name = "ADD " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + R2reg(Rn);
            break;
        case 1:
            op_name = "SUB " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + R2reg(Rn);
            break;
        case 2:
            op_name = "ADD " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + std::to_string(Rn);
            break;
        case 3:
            op_name = "SUB " + R2reg(Rd) + ", " + R2reg(Rs) + ", " + std::to_string(Rn);
            break;
    }
    return op_name;
}

std::string mcas_imm(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t op = opcode & 0x1800;
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t nn = opcode & 0x00FF;

    switch(op) {
        case 0x0000:
            op_name = "MOV " + R2reg(Rd) + ", " + std::to_string(nn);
            break;
        case 0x0800:
            op_name = "CMP " + R2reg(Rd) + ", " + std::to_string(nn);
            break;
        case 0x1000:
            op_name = "ADD " + R2reg(Rd) + ", " + std::to_string(nn);
            break;
        case 0x1800:
            op_name = "SUB " + R2reg(Rd) + ", " + std::to_string(nn);
            break;
    }
    return op_name;
}

std::string ld_pc(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool L = bit_get(opcode, 11);
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t imm = (opcode & 0x00FF) << 2;

    if(L) {
        op_name = "LDR " + R2reg(Rd) + ", [PC, " + std::to_string(imm) + "]";
    }
    else {
        op_name = "STR " + R2reg(Rd) + ", [PC, " + std::to_string(imm) + "]";
    }
    return op_name;
}

std::string hi_reg(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = (opcode & 0x03C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);

    switch(Op) {
        case 1:
            op_name = "ADD " + R2reg(Rd) + ", " + R2reg(Rs + 8);
            break;
        case 2:
            op_name = "ADD " + R2reg(Rd + 8) + ", " + R2reg(Rs);
            break;
        case 3:
            op_name = "ADD " + R2reg(Rd + 8) + ", " + R2reg(Rs + 8);
            break;
        case 5:
            op_name = "CMP " + R2reg(Rd) + ", " + R2reg(Rs + 8);
            break;
        case 6:
            op_name = "CMP " + R2reg(Rd + 8) + ", " + R2reg(Rs);
            break;
        case 7:
            op_name = "CMP " + R2reg(Rd + 8) + ", " + R2reg(Rs + 8);
            break;
        case 9:
            op_name = "MOV " + R2reg(Rd) + ", " + R2reg(Rs + 8);
            break;
        case 10:
            op_name = "MOV " + R2reg(Rd + 8) + ", " + R2reg(Rs);
            break;
        case 11:
            op_name = "MOV " + R2reg(Rd + 8) + ", " + R2reg(Rs + 8);
            break;
        case 12:
            op_name = "BX " + R2reg(Rs);
            break;
        case 13:
            op_name = "BX " + R2reg(Rs + 8);
            break;
        default:
            op_name = "Invalid!";
            break;
    }
    return op_name;
}

std::string alu(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = (opcode & 0x03C0) >> 6;
    uint16_t Rs = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);

    switch(Op) {
        case 0:
            op_name = "AND " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 1:
            op_name = "EOR " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 2:
            op_name = "LSL " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 3:
            op_name = "LSR " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 4:
            op_name = "ASR " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 5:
            op_name = "ADC " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 6:
            op_name = "SBC " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 7:
            op_name = "ROR " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 8:
            op_name = "TST " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 9:
            op_name = "NEG " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 10:
            op_name = "CMP " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 11:
            op_name = "CMN " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 12:
            op_name = "ORR " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 13:
            op_name = "MUL " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 14:
            op_name = "BIC " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
        case 15:
            op_name = "MVN " + R2reg(Rd) + ", " + R2reg(Rs);
            break;
    }
    return op_name;
}

std::string ls_ext(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = (opcode & 0x0C00);
    uint16_t Ro = (opcode & 0x01C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = opcode & 0x0007;

    switch(Op) {
        case 0x000:
            op_name = "STRH " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0x400:
            op_name = "LDSB " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0x800:
            op_name = "LDRH " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0xC00:
            op_name = "LDSH " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
    }
    return op_name;
}

std::string ls_reg(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = opcode & 0x0C00;
    uint16_t Ro = (opcode & 0x01C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);

    switch(Op) {
        case 0x0000:
            op_name = "STR " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0x0400:
            op_name = "STRB " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0x0800:
            op_name = "LDR " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
        case 0x0C00:
            op_name = "LDRB " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + R2reg(Ro) + "]";
            break;
    }
    return op_name;
}

std::string ls_imm(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = opcode & 0x1800;
    uint16_t imm = (opcode & 0x07C0) >> 6;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);

    switch(Op) {
        case 0x0000:
            op_name = "STR " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm << 2) + "]";
            break;
        case 0x0800:
            op_name = "LDR " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm << 2) + "]";
            break;
        case 0x1000:
            op_name = "STRB " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm) + "]";
            break;
        case 0x1800:
            op_name = "LDRB " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm) + "]";
            break;
    }
    return op_name;
}

std::string ls_hw(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool L = bit_get(opcode, 11);
    uint16_t imm = ((opcode & 0x07C0) >> 6) << 1;
    uint16_t Rb = (opcode & 0x0038) >> 3;
    uint16_t Rd = (opcode & 0x0007);

    if(L) {
        op_name = "LDRH " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm) + "]";
    }
    else {
        op_name = "STRH " + R2reg(Rd) + ", [" + R2reg(Rb) + ", " + std::to_string(imm) + "]";
    }
    return op_name;
}

std::string ls_sp(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool L = bit_get(opcode, 11);
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t imm = (opcode & 0x00FF) << 2;

    if(L) {
        op_name = "LDR " + R2reg(Rd) + ", [SP, " + std::to_string(imm) + "]";
    }
    else {
        op_name = "STR " + R2reg(Rd) + ", [SP, " + std::to_string(imm) + "]";
    }
    return op_name;
}

std::string ld(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool SP = bit_get(opcode, 11);
    uint16_t Rd = (opcode & 0x0700) >> 8;
    uint16_t imm = (opcode & 0x00FF) << 2;

    if(SP) {
        op_name = "ADD " + R2reg(Rd) + ", SP, " + std::to_string(imm);
    }
    else {
        op_name = "ADD " + R2reg(Rd) + ", PC, " + std::to_string(imm);
    }
    return op_name;
}

std::string push_pop(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool L = bit_get(opcode, 11);
    bool R = bit_get(opcode, 8);
    uint16_t imm = opcode & 0x00FF;

    if(L) {
        if(R) {
            op_name = "POP {" + std::to_string(imm) + ", PC}";
        }
        else {
            op_name = "POP {" + std::to_string(imm) + "}";
        }
    }
    else {
        if(R) {
            op_name = "PUSH {" + std::to_string(imm) + ", LR}";
        }
        else {
            op_name = "PUSH {" + std::to_string(imm) + "}";
        }
    }
    return op_name;
}

std::string sp_ofs(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool S = bit_get(opcode, 7);
    uint16_t offset = (opcode & 0x007F) << 2;

    if(S) {
        offset *= -1;
    }
    op_name = "ADD SP, " + std::to_string(offset);

    return op_name;
}

std::string ls_mp(uint16_t opcode)
{
    std::string op_name = "Undefined";
    bool L = bit_get(opcode, 11);
    uint16_t Rb = (opcode & 0x0700) >> 8;
    uint16_t imm = opcode & 0x00FF;

    if(L) {
        op_name = "LDMIA " + R2reg(Rb) + "!, {" + std::to_string(imm) + "}";
    }
    else {
        op_name = "STMIA " + R2reg(Rb) + "!, {" + std::to_string(imm) + "}";
    }
    return op_name;
}

std::string b_cond(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t Op = (opcode & 0x0F00) >> 8;
    int32_t offset = (opcode & 0x00FF) << 1;
    offset = sign_extend32(offset, 9);

    switch(Op) {
        case 0:
            op_name = "BEQ " + std::to_string(offset);
            break;
        case 1:
            op_name = "BNE " + std::to_string(offset);
            break;
        case 2:
            op_name = "BCS " + std::to_string(offset);
            break;
        case 3:
            op_name = "BCC " + std::to_string(offset);
            break;
        case 4:
            op_name = "BMI " + std::to_string(offset);
            break;
        case 5:
            op_name = "BPL " + std::to_string(offset);
            break;
        case 6:
            op_name = "BVS " + std::to_string(offset);
            break;
        case 7:
            op_name = "BVC " + std::to_string(offset);
            break;
        case 8:
            op_name = "BHI " + std::to_string(offset);
            break;
        case 9:
            op_name = "BLS " + std::to_string(offset);
            break;
        case 10:
            op_name = "BGE " + std::to_string(offset);
            break;
        case 11:
            op_name = "BLT " + std::to_string(offset);
            break;
        case 12:
            op_name = "BGT " + std::to_string(offset);
            break;
        case 13:
            op_name = "BLE " + std::to_string(offset);
            break;
        case 15:
            op_name = "SWI";
            break;
    }
    return op_name;
}

std::string b_uncond(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint16_t offset = (opcode & 0x7FF) << 1;
    offset = sign_extend32(offset, 12);

    op_name = "B " + std::to_string(offset);
    return op_name;
}

std::string bl(uint16_t opcode)
{
    std::string op_name = "Undefined";
    uint32_t imm = (opcode & 0x7FF) << 12;
    if(bit_get(opcode, 11)) {
        op_name = "BL(L) " + std::to_string(imm);
    }
    else {
        op_name = "BL(H) " + std::to_string(imm);
    }
    return op_name;
}
