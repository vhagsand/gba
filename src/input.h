#pragma once

#include "memory.h"

enum Keys {
    A,
    B,
    SELECT,
    START,
    RIGHT,
    LEFT,
    UP,
    DOWN,
    R,
    L
};

class Input {
public:
    Input(Memory &mem)
        : mem(mem) {
    }
    void set_key(int key);
    void clear_key(int key);

private:
    Memory &mem;

    void handle_Interrupts(uint16_t keys);
};