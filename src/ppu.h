#pragma once

#include "memory.h"

#define BG_PALETTE 0x05000000
#define OB_PALETTE 0x05000200
#define VRAM       0x06000000
#define OVRAM      0x06010000
#define OAM        0x07000000
class Ppu {
    enum Ppu_states {
        DRAW,
        HBLANK,
        VBLANK_DRAW,
        VBLANK_HBLANK
    };

    public:
    Ppu(Memory &mem)
        : mem(mem) {
    }
    bool step(int cycles);
    uint16_t* getPixels();

private:
    Memory &mem;
    int cycle_count = 0;
    uint8_t line_count = 0;
    Ppu_states current_state = DRAW;
    uint16_t dispstat;
    uint16_t* screen_buffer = new uint16_t[240 * 160];

    void set_line(int count);
    void draw_mode_0(uint16_t dispcnt);
    void draw_mode_1(uint16_t dispcnt);
    void draw_mode_2(uint16_t dispcnt);
    void draw_mode_3();
    void draw_mode_4(uint16_t dispcnt);
    void draw_mode_5(uint16_t dispcnt);
    void draw_tiles(int bg_index);
    void draw_tiles_aff(int bg_index);
    uint16_t draw_256_1(uint16_t tile, uint32_t tile_data, uint16_t y_in_tile, uint32_t x_in_tile);
    uint16_t draw_16_16(uint16_t tile, uint32_t tile_data, uint16_t y_in_tile, uint32_t x_in_tile);
    void draw_sprites(uint64_t sprites[], int length, bool one_dimensional);
};