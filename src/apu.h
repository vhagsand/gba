#if !defined(APU_H)
#define APU_H

#include "gsl/gsl"
#include "dma.h"

enum ChannelType {
    square1,
    square2,
    wave,
    noise
};

// TODO: What should the init values be?
struct Square {
    ChannelType type;
    uint8_t *reg0; // NRx0
    uint8_t *reg1; // NRx1
    uint8_t *reg2; // NRx2
    uint8_t *reg3; // NRx3
    uint8_t *reg4; // NRx4
    uint8_t *status;    // NR52
    uint8_t *wave_table;
    // Timer
    int timer_period = 4 * (2048 - 1524);
    int timer_val = 4 * (2048 - 1524);
    // Duty (triggered by timer)
    int duty_index = 0;
    // Length counter
    int length_counter = 0;
    // Envelope
    int vol_envelope_timer = 7;
    int volume = 15;
    // Sweep
    int sweep_timer = 7;
    bool sweep_enabled = false;
    unsigned int frequency_shadow = 0;
    // Wave (triggered by timer)
    uint8_t sample_buffer = 0;
    uint8_t position = 0;
    // LFSR (triggered by timer)
    uint16_t lfsr = 0;

    void disable()
    {
        const uint8_t status_bit = ~(1 << type);
        *status = *status & status_bit;
    }

    void enable()
    {
        const uint8_t status_bit = (1 << type);
        *status = *status | status_bit;
    }

    uint8_t read_wave_sample()
    {
        int byte = position / 2;
        int nibble = position % 2;
        uint8_t data = wave_table[byte];
        if (nibble == 0) {
            return data >> 4;
        } else {
            return data & 0x0f;
        }
    }
};

class APU {
public:

    APU(gsl::span<uint8_t, 168> regs, Dma* dma1, Dma* dma2)
        : registers(regs),
        dma1(dma1),
        dma2(dma2) { }

    void load_length_counter_square1(int len)
    {
        square1.length_counter = len;
    }

    void load_length_counter_square2(int len)
    {
        square2.length_counter = len;
    }

    // void load_length_counter_wave(int len)
    // {
    //     wave.length_counter = len;
    // }

    void load_length_counter_noise(int len)
    {
        noise.length_counter = len;
    }

    void advance(int cycles);
    void trigger_square1();
    void trigger_square2();
    // void trigger_wave();
    void trigger_noise();

    int ds_a_timer();
    int ds_b_timer();
    void step_ds_a();
    void step_ds_b();
    void reset_fifo_a();
    void reset_fifo_b();
    void load_fifo_a(uint8_t data);
    void load_fifo_b(uint8_t data);
    float output();

private:
    gsl::span<uint8_t, 168> registers;
    Dma* dma1;
    Dma* dma2;

    int frame_sequencer = 0;
    int frame_sequencer_step = 0;

    Square square1 = {
        ChannelType::square1,
        &registers[NR10],
        &registers[NR11],
        &registers[NR12],
        &registers[NR13],
        &registers[NR14],
        &registers[NR52],
        &registers[wave_table_begin]
    };
    Square square2 = {
        ChannelType::square2,
        nullptr,
        &registers[NR21],
        &registers[NR22],
        &registers[NR23],
        &registers[NR24],
        &registers[NR52],
        &registers[wave_table_begin]
    };
    // Square wave = {
    //     ChannelType::wave,
    //     &registers[NR30],
    //     &registers[NR52],
    //     &registers[wave_table_begin]
    // };
    Square noise = {
        ChannelType::noise,
        nullptr,
        &registers[NR41],
        &registers[NR42],
        &registers[NR43],
        &registers[NR44],
        &registers[NR52],
        &registers[wave_table_begin]
    };

    int8_t direct_sound_a_buffer[16];
    int8_t direct_sound_b_buffer[16];
    int direct_sound_a_counter = 0;
    int direct_sound_b_counter = 0;
    int8_t direct_sound_a_out = 0;
    int8_t direct_sound_b_out = 0;

    static constexpr int NR10 = 0x60;
    static constexpr int NR11 = 0x62;
    static constexpr int NR12 = 0x63;
    static constexpr int NR13 = 0x64;
    static constexpr int NR14 = 0x65;

    static constexpr int NR20 = 5; // not used
    static constexpr int NR21 = 0x68; // duty, len
    static constexpr int NR22 = 0x69; // vol, envelope, period
    static constexpr int NR23 = 0x6c; // freq low
    static constexpr int NR24 = 0x6d; // trigger, len enable, freq high

    static constexpr int NR30 = 10;
    static constexpr int NR31 = 11;
    static constexpr int NR32 = 12;
    static constexpr int NR33 = 13;
    static constexpr int NR34 = 14;

    static constexpr int NR40 = 15; // not used
    static constexpr int NR41 = 0x78;
    static constexpr int NR42 = 0x79;
    static constexpr int NR43 = 0x7c;
    static constexpr int NR44 = 0x7d;

    static constexpr int NR50 = 20;
    static constexpr int NR51 = 21;
    static constexpr int NR52 = 0x84; // power control, length statuses

    static constexpr int SOUNDCNT_H = 0x82;
    static constexpr int FIFO_A = 0xa0; // 4 bytes
    static constexpr int FIFO_B = 0xa4; // 4 bytes

    static constexpr int wave_table_begin = 32;
};

#endif