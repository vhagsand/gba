#include <iostream>
#include "timer.h"
#include "utils.h"

void Timer::step(int cycles)
{
    if(enabled && !count_up) { //Timer enabled and no count up
        switch(prescale) {
            case 0:
                increment(cycles);
                break;
            case 1:
                prescale_cnt += cycles;
                if(prescale_cnt > 64) {
                    prescale_cnt -= 64;
                    increment(1);
                }
                break;
            case 2:
                prescale_cnt += cycles;
                if(prescale_cnt > 256) {
                    prescale_cnt -= 256;
                    increment(1);
                }
                break;
            case 3:
                prescale_cnt += cycles;
                if(prescale_cnt > 1024) {
                    prescale_cnt -= 1024;
                    increment(1);
                }
                break;
        }
    }
}

void Timer::step_count_up(int cycles)
{
    if(enabled) {
        increment(cycles);
    }
}

void Timer::increment(int cycles)
{
    counter += cycles;
    if(counter > UINT16_MAX) { //Overflow
        counter -= UINT16_MAX;
        counter += start_time;
        if((count_up_timer != nullptr) && count_up_timer->count_up) {
            count_up_timer->step_count_up(1);
        }
        if(irq) {
            uint16_t iflags = *irq_register;
            iflags = bit_set(iflags, timer_index + 3);
            *irq_register = iflags;
        }
        if(apu && apu->ds_a_timer() == timer_index) {
            apu->step_ds_a();
        }
        if(apu && apu->ds_b_timer() == timer_index) {
            apu->step_ds_b();
        }
    }
    *(uint16_t *)memory = (uint16_t)counter; //TMxCNT_L
}

void Timer::set_start_time(uint16_t value, bool high_byte)
{
    if(high_byte) {
        start_time &= 0xFF;
        start_time |= value << 8;
    }
    else {
        start_time &= 0xFF00;
        start_time |= value;
    }
}

void Timer::set_control(uint16_t value)
{
    prescale = value & 0x3;
    count_up = bit_get(value, 2);
    irq = bit_get(value, 6);
    enabled = bit_get(value, 7);
    if(enabled && (enabled != old_enabled)) {
        counter = start_time;
        *(uint16_t *)memory = start_time; //TMxCNT_L
        old_enabled = enabled;
    }
}