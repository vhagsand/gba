#include "flash.h"
#include "memory.h"
#include <iostream>

// TODO:
// -Test!
// -128k support

void Flash::write(uint32_t addr, uint8_t value)
{
    if(flash_mode == WRITE) {
        if(flash_bank == 1) {
            addr |= 0x10000;
        }
        memory_ptr->write8(addr, value);
        flash_mode = READY;
    }
    else if(flash_mode == BANK) {
        flash_bank = value;
        flash_mode = READY;
    }
    else {
        if(command_id == 0 && value == 0xAA) {
            command_id = 1;
        }
        else if(command_id == 1 && value == 0x55) {
            command_id = 2;
        }
        else if (command_id == 2) {
            switch(value) {
                case 0x10: //Erase chip
                    if(flash_mode == ERASE)
                    {
                        erase();
                        flash_mode = READY;
                    }
                    break;
                case 0x30: //Erase 4k sector
                    if(flash_mode == ERASE)
                    {
                        erase_sector(addr);
                        flash_mode = READY;
                    }
                    break;
                case 0x80: //Prepare for erase
                    flash_mode = ERASE;
                    break;
                case 0x90: //Enter id mode
                    id_mode = true;
                    break;
                case 0xA0: //Prepare write
                    flash_mode = WRITE;
                    break;
                case 0xB0: //Set memory bank
                    flash_mode = BANK;
                    break;
                case 0xF0: //Leave id mode
                    id_mode = false;
                    break;
            }
            command_id = 0;
        }
        else {
            command_id = 0;
        }
    }
}

uint8_t Flash::read(uint32_t addr)
{
    if(flash_bank == 1) {
        addr |= 0x10000;
    }
    if(id_mode) {
        if(addr == 0x0E000000) {
            return 0x62;
        }
        else if(addr == 0x0E000001) {
            return 0x13;
        }
    }
    return 0xFF;//memory_ptr->read8(addr);
}

void Flash::erase()
{
    for(int i = 0xE000000; i <= 0xE00FFFF; i++){
        memory_ptr->set8(i, 0xFF);
    }
}

void Flash::erase_sector(uint32_t addr)
{
    int addr_end = addr + 0xFFF;
    for(int i = addr; i <= addr_end; i++){
        memory_ptr->set8(i, 0xFF);
    }
}