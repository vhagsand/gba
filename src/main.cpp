#include <iostream>
#include <fstream>
#include <math.h>
#include <SDL2/SDL.h>
#if defined(__APPLE__)
#include <SDL2_ttf/SDL_ttf.h>
#else
#include <SDL_ttf.h>
#endif
#include "main.h"
#include "debug.h"
#include "cpu.h"
#include "timer.h"
#include "ppu.h"
#include "apu.h"
#include "input.h"
#include "dma.h"

/*
    Resources:
      https://eb2.co/blog/2014/04/c--14-and-sdl2-managing-resources/

    TODO:
    -Opcode bugs
    -PPU (see ppu.c)
    -EEPROM
    -"Open bus"
*/

int main(int argc, char *argv[])
{
    //SDL stuff
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO);
    TTF_Init();

    SDL_GameController *controller = nullptr;
    controller = init_controller(controller);

    main_window = SDL_CreateWindow("gba", 50, 100, gba_width * gba_scale,
        gba_height * gba_scale, SDL_WINDOW_OPENGL);
    main_render = SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED);
#ifdef GBA_DEBUG
    debug_window = SDL_CreateWindow("debug (paused)", 820, 100, 480, 720, SDL_WINDOW_OPENGL);
    debug_render = SDL_CreateRenderer(debug_window, -1, SDL_RENDERER_ACCELERATED);
#endif
    SDL_Texture *texture = SDL_CreateTexture(main_render, SDL_PIXELFORMAT_BGR555,
        SDL_TEXTUREACCESS_STREAMING, gba_width, gba_height);

    // Audio stuff
    SDL_AudioSpec desired = {};
    SDL_AudioSpec obtained = {};

    desired.freq = 48000;
    desired.format = AUDIO_F32;
    desired.channels = 1;
    desired.samples = 64;
    desired.callback = audio_handler;

    SDL_AudioDeviceID device = SDL_OpenAudioDevice(
        nullptr,
        0,
        &desired,
        &obtained,
        0
    );

    assert(device); // TODO: Handle error

    //GBA stuff
    bool start_bios = false;
    Cpu cpu(mem);
    Ppu ppu(mem);
    input = new Input(mem);

    if(argc >= 2) {
        for(int i = 1; i < argc; i++) {
            if(strcmp(argv[i], "-b") == 0) {
                start_bios = true;
            }
            else if(strcmp(argv[i], "-pc") == 0) {
                i++;
                break_address = std::stoi(argv[i]);
            }
            else if(strcmp(argv[i], "-ns") == 0) {
                no_sound = true;
            }
            else {
                mem.load_rom(argv[i]);
                std::string path = argv[i];
                std::string filename = path.substr(path.find_last_of("/\\") + 1);
                std::string::size_type const p(filename.find_last_of('.'));
                filename = filename.substr(0, p);
                title = "gba: " + filename;
                SDL_SetWindowTitle(main_window, title.c_str());
                mem.ram_save_filename = "saves/" + filename + ".sav";
                mem.load_ram();
            }
        }
    }
    mem.load_bios();

    if(!start_bios) {
        cpu.init_no_bios();
    }

    Dma dma0(0, mem.irq_flags(), &mem);
    mem.dma_set(0, &dma0);
    Dma dma1(1, mem.irq_flags(), &mem);
    mem.dma_set(1, &dma1);
    Dma dma2(2, mem.irq_flags(), &mem);
    mem.dma_set(2, &dma2);
    Dma dma3(3, mem.irq_flags(), &mem);
    mem.dma_set(3, &dma3);

    APU apu(mem.sound_regs(), &dma1, &dma2);
    mem.set_apu(&apu);

    Timer timer3(3, nullptr, mem.timer_reg(), mem.irq_flags());
    mem.timer_set(3, &timer3);
    Timer timer2(2, &timer3, mem.timer_reg(), mem.irq_flags());
    mem.timer_set(2, &timer2);
    Timer timer1(1, &timer2, mem.timer_reg(), mem.irq_flags(), &apu);
    mem.timer_set(1, &timer1);
    Timer timer0(0, &timer1, mem.timer_reg(), mem.irq_flags(), &apu);
    mem.timer_set(0, &timer0);

#ifdef GBA_DEBUG
    Debug debug(debug_render, mem, cpu);
    draw_debug(debug);
#endif

    // Unpause
    if(!no_sound) {
        SDL_PauseAudioDevice(device, 0);
    }

    // 16.78Mhz and a sample rate of 48000 means 349.58 cycles/sample
    const int cycles_per_sample = 340; // keep it on the low side to avoid pops
    int cycles_since_last_sample = 0;

    // Use 60 fps update
    constexpr float step_length = 1.0 / 60.0;
    float accumulated_time = 0.0;
    uint32_t prev_time = SDL_GetTicks();
    int quadricycle_fragments = 0;
    float frame_cnt = 0;

    while (!quit) {
        // Calculate time spent drawing
        uint32_t time = SDL_GetTicks();
        accumulated_time += (time - prev_time) / 1000.0;
        prev_time = time;

        //GBA Logic
#ifdef GBA_DEBUG
        while((!pause || step) && !redraw && !sound_buffer.is_full()) {
#else
        while(!redraw && !sound_buffer.is_full()) {
#endif
            int cycles = cpu.step();
            cycles_since_last_sample += cycles;

            timer0.step(cycles);
            timer1.step(cycles);
            timer2.step(cycles);
            timer3.step(cycles);
            redraw = ppu.step(cycles);
            // APU uses one quarter the clock frequency
            quadricycle_fragments += cycles;
            apu.advance(quadricycle_fragments / 4);
            quadricycle_fragments &= 3;

            if (cycles_since_last_sample >= cycles_per_sample) {
                cycles_since_last_sample -= cycles_per_sample;
                float out = apu.output();
                sound_buffer.push_back(out);
            }

#ifdef GBA_DEBUG
            if(step) {
                draw_debug(debug);
                step = false;
            }
            if(break_address == cpu.reg_raw(PC, M_USER)) {
                last_pause = pause;
                pause = true;
                SDL_SetWindowTitle(debug_window, "debug (paused)");
            }
#endif
        }
#ifdef GBA_DEBUG
        if((pause != last_pause)) {
            draw_debug(debug);
            last_pause = pause;
        }
#endif
        handle_events();

        if (accumulated_time > step_length) {
            // Draw if its time and ppu is ready
            SDL_RenderClear(main_render);
            texture = draw_main(ppu.getPixels(), texture);
            SDL_RenderCopy(main_render, texture, NULL, NULL);
            SDL_RenderPresent(main_render);
            redraw = false;
            frame_cnt += accumulated_time;

            if(frame_cnt > 0.25) { //Update frame counter 4 times/s
                frame_cnt = 0;
                std::string frames = std::to_string((int)round(1 / accumulated_time));
                SDL_SetWindowTitle(main_window, (title + " - " + frames + "fps").c_str());
            }
            accumulated_time = 0;
        }
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(main_render);
    SDL_DestroyWindow(main_window);
#ifdef GBA_DEBUG
    SDL_DestroyRenderer(debug_render);
    SDL_DestroyWindow(debug_window);
#endif
    TTF_Quit();
    SDL_Quit();

    return 0;
}

void quit_app()
{
    mem.save_ram();
    quit = true;
}

void audio_handler(
    [[maybe_unused]] void* userdata,
    uint8_t* stream,
    int len)
{
    size_t nr_of_samples = len / 4;
    if (sound_buffer.size() >= nr_of_samples) {
        auto chunk = sound_buffer.take_front(nr_of_samples);
        for (size_t i = 0; i < nr_of_samples; ++i) {
            *((float*)stream + i) = chunk[i];
        }
    } else {
        std::fill(stream, stream + len, -1.0f);
    }
}

SDL_Texture* draw_main(uint16_t *screen_buffer, SDL_Texture *texture)
{
    Uint16* pixels = nullptr;
    int pitch = 0;

    if (SDL_LockTexture(texture, nullptr, (void**)&pixels, &pitch)) {
        // If the locking fails, you might want to handle it somehow. SDL_GetError(); or something here.
    }

    int gba_size = gba_width * gba_height;
    for(int i = 0; i < (gba_size); i++) {
        pixels[i] = screen_buffer[i];
    }

    SDL_UnlockTexture(texture);
    return texture;
}

void draw_debug(Debug& debug)
{
    SDL_RenderClear(debug_render);
    debug.draw();
    SDL_RenderPresent(debug_render);
}

void handle_events()
{
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_QUIT:
                quit_app();
                break;
            case SDL_WINDOWEVENT:
                if(event.window.event == SDL_WINDOWEVENT_CLOSE) {
                    quit_app();
                }
                break;
            case SDL_KEYDOWN:
                hande_keys_down(event.key.keysym.sym);
                break;
            case SDL_CONTROLLERBUTTONDOWN:
                hande_keys_down(event.cbutton.button);
                break;
            case SDL_KEYUP:
                hande_keys_up(event.key.keysym.sym);
                break;
            case SDL_CONTROLLERBUTTONUP:
                hande_keys_up(event.cbutton.button);
                break;
        }
    }
}

void hande_keys_down(int keycode)
{
    switch(keycode) {
        case SDLK_SPACE:
            pause_toggle();
            break;
        case SDLK_s:
            step = true;
            break;
        case SDLK_p:
            dump_mem();
            break;
        case SDLK_DOWN:
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
            input->set_key(DOWN);
            break;
        case SDLK_UP:
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
            input->set_key(UP);
            break;
        case SDLK_LEFT:
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
            input->set_key(LEFT);
            break;
        case SDLK_RIGHT:
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
            input->set_key(RIGHT);
            break;
        case SDLK_q:
        case SDL_CONTROLLER_BUTTON_BACK:
            input->set_key(SELECT);
            break;
        case SDLK_w:
        case SDL_CONTROLLER_BUTTON_START:
            input->set_key(START);
            break;
        case SDLK_z:
        case SDL_CONTROLLER_BUTTON_A:
            input->set_key(A);
            break;
        case SDLK_x:
        case SDL_CONTROLLER_BUTTON_B:
            input->set_key(B);
            break;
        case SDLK_c:
        case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
            input->set_key(L);
            break;
        case SDLK_v:
        case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
            input->set_key(R);
            break;
    }
}

void hande_keys_up(int keycode)
{
    switch(keycode) {
        case SDLK_DOWN:
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
            input->clear_key(DOWN);
            break;
        case SDLK_UP:
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
            input->clear_key(UP);
            break;
        case SDLK_LEFT:
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
            input->clear_key(LEFT);
            break;
        case SDLK_RIGHT:
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
            input->clear_key(RIGHT);
            break;
        case SDLK_q:
        case SDL_CONTROLLER_BUTTON_BACK:
            input->clear_key(SELECT);
            break;
        case SDLK_w:
        case SDL_CONTROLLER_BUTTON_START:
            input->clear_key(START);
            break;
        case SDLK_z:
        case SDL_CONTROLLER_BUTTON_A:
            input->clear_key(A);
            break;
        case SDLK_x:
        case SDL_CONTROLLER_BUTTON_B:
            input->clear_key(B);
            break;
        case SDLK_c:
        case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
            input->clear_key(L);
            break;
        case SDLK_v:
        case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
            input->clear_key(R);
            break;
    }
}

void pause_toggle()
{
    last_pause = pause;
    pause = !pause;
    if(pause) {
        SDL_SetWindowTitle(debug_window, "debug (paused)");
    }
    else {
        SDL_SetWindowTitle(debug_window, "debug (running)");
    }
}

void dump_mem()
{
    std::ofstream myfile ("mem_dump.txt");
    if (myfile.is_open()) {
        //WRAM-onboard
        for(int i = 0x2000000; i <= 0x203FFFF; i ++) {
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        //WRAM-onchip
        for(int i = 0x3000000; i <= 0x3007FFF; i ++) {
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        //IO
        for(int i = 0x4000000; i <= 0x40003FF; i ++) {
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        //BG/OBJ
        for(int i = 0x5000000; i <= 0x50003FF; i ++) {
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        //VRAM
        for(int i = 0x6000000; i <= 0x6017FFF; i ++) {
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        //OAM
        for(int i = 0x7000000; i <= 0x70003FF; i ++){
            myfile << std::hex << i << " " << (uint16_t)mem.read8(i) << "\n";
        }
        myfile.close();
    }
    else {
        std::cout << "Unable to open file";
    }
}

SDL_GameController* init_controller(SDL_GameController *controller)
{
    for (int i = 0; i < SDL_NumJoysticks(); ++i) {
        if (SDL_IsGameController(i)) {
            controller = SDL_GameControllerOpen(i);
            if (controller) {
                break;
            }
        }
    }
    return controller;
}
