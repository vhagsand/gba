#pragma once

#include "memory.h"

enum Regs {
    R0 = 0,
    R1 = 1,
    R2 = 2,
    R3 = 3,
    R4 = 4,
    R5 = 5,
    R6 = 6,
    R7 = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,
    SP = 13,
    LR = 14,
    PC = 15,
    CPSR = 16,
    SPSR = 17
};

enum Flags {
    N = 31,
    Z = 30,
    C = 29,
    V = 28,
    IRQ = 7,
    FIQ = 6,
    STATE = 5
};

enum Modes {
    M_USER = 16,
    M_FIQ = 17,
    M_IRQ = 18,
    M_SUPERVISOR = 19,
    M_ABORT = 23,
    M_UNDEFINED = 27,
    M_SYSTEM = 31
};

class Cpu {
public:
    Cpu(Memory &mem)
        : mem(mem) {
        init();
    }

    bool flag_get(Flags flag);
    void flag_set(Flags flag, bool on);
    Modes mode_get();
    void mode_set(Modes mode);
    uint32_t reg_get(uint32_t reg);
    uint32_t reg_raw(Regs reg, Modes mode);
    void reg_set(uint32_t reg, uint32_t value);
    int step();
    void init_no_bios();

private:
    Memory &mem;
    bool thumb = false;
    uint32_t regs[18][15] = {{0}};

    void init();
    void exec_interrupts();
    int exec_arm();
    int exec_thumb();
    //ARM
    int mul_mla(uint32_t opcode);
    int mull_mlal(uint32_t opcode);
    int hw_transfer(uint32_t opcode);
    int swap(uint32_t opcode);
    int bx(uint32_t opcode);
    int alu(uint32_t opcode, bool I);
    int ldr(uint32_t opcode, bool I);
    int ldm_stm(uint32_t opcode);
    int b_bl(uint32_t opcode);
    int swi();
    //Thumb
    int shift(uint16_t opcode);
    int add_sub(uint16_t opcode);
    int mcas_imm(uint16_t opcode);
    int hi_reg(uint16_t opcode);
    int alu(uint16_t opcode);
    int ld_pc(uint16_t opcode);
    int ls_ext(uint16_t opcode);
    int ls_reg(uint16_t opcode);
    int ls_imm(uint16_t opcode);
    int ls_hw(uint16_t opcode);
    int ls_sp(uint16_t opcode);
    int ld(uint16_t opcode);
    int push_pop(uint16_t opcode);
    int sp_ofs(uint16_t opcode);
    int ls_mp(uint16_t opcode);
    int b_cond(uint16_t opcode);
    int b_uncond(uint16_t opcode);
    int bl(uint16_t opcode);
    //Misc
    void setZNArmAlu(uint32_t Rd, uint32_t res);
    uint32_t reg_shift(uint32_t opcode, bool *logic_carry);
    uint32_t lsl(uint32_t shift, uint32_t value, bool *logic_carry);
    uint32_t lsr(uint32_t shift, uint32_t value, bool *logic_carry);
    uint32_t asr(uint32_t shift, uint32_t value, bool *logic_carry);
    uint32_t ror(uint32_t shift, uint32_t value, bool *logic_carry);
    uint32_t rol32(uint32_t number, uint32_t count);
    uint32_t ror32(uint32_t number, uint32_t count);
};